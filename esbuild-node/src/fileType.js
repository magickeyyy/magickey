const fs = require('fs');
const path = require('path');
const chalk = require('chalk').default;
const { fileTypeFromStream } = require('file-type');
const chalkAnimation = require('chalk-animation').default;

const png = fs.createReadStream(
    path.join(process.cwd(), 'src/images/comment.png'),
);
fileTypeFromStream(png).then((value) => {
    // console.log('png:', value);
    // console.log(
    //     chalk.green(
    //         'I am a green line ' +
    //             chalk.blue.underline.bold('with a blue substring') +
    //             ' that becomes green again!',
    //     ),
    // );
});
// chalkAnimation.rainbow('Lorem ipsum dolor sit amet');
