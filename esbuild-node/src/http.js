const { default: chalk } = require('chalk');
const fs = require('fs');
const { CWD, BigTextPath } = require('./utils');

const http = require('http');
const server = http.createServer();

server.on('request', (req, res) => {
    const src = fs.createReadStream(BigTextPath);
    src.pipe(res);
});

server.listen(3000, () => {
    console.log(chalk.blue(`http://localhost:3000`));
});
