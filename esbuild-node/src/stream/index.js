const fs = require('fs');
const path = require('path');

const CWD = process.cwd();

function wirteStream(src, dest, options) {
    return new Promise((resolve, reject) => {
        const from = fs.createReadStream(src, { highWaterMark: 1024 });
        const to = fs.createWriteStream(dest);
        from.on('error', (error) => {
            from.destroy();
            to.destroy();
            reject(error);
        });
        to.on('error', (error) => {
            from.destroy();
            to.destroy();
            reject(error);
        });
        to.on('finish', resolve);
        if (options) {
            options.src = from;
            options.dest = to;
            options.hook?.();
        }
        from.pipe(to);
    });
}

const png = path.resolve(CWD, 'src/images/comment.png');
const png2 = path.resolve(CWD, 'src/images/comment2.png');

wirteStream(png, png2);
