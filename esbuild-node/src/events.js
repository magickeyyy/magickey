const { EventEmitter } = require('events');

const emitter = new EventEmitter();
const emittere = new EventEmitter({ captureRejections: true });
const symbol = Symbol('event emitter 测试');
emitter.on(symbol, function () {
    console.log('symbol:', 1, this === emitter);
});
emitter.on(symbol, () => {
    console.log('symbol:', 2, this === emitter);
});
emittere.on(symbol, function (...args) {
    console.log('emittere symbol:', this === emittere, ...args);
});

emittere.on(symbol, (...args) => {
    console.log('emittere symbol:', this === emittere, ...args);
});
emittere.on(symbol, async function (...args) {
    console.log(':', args[0] instanceof Error, emittere.listenerCount(symbol));
});
emittere.on('error', function (error) {
    console.log('emittere error:', emittere.listenerCount);
});

// emitter.emit(symbol);
// emittere.emit(symbol, new Error(222));
