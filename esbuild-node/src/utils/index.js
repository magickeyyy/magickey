const fs = require('fs');
const p_fs = require('fs/promises');
const path = require('path');

const CWD = process.cwd();
const BigTextPath = path.join(CWD, 'dustbin', '.big_text.text');
const BigTextPath2 = path.join(CWD, 'dustbin', '.big_text2.text');

class StatStopTream {
    stream = fs.createWriteStream(BigTextPath, { flags: 'ax' });
    writeable = true;
    text =
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n';
    constructor({ taskLength, total }) {
        this.taskLength = taskLength;
        this.total = total;
        this.rest = total;
        this.write = this.write.bind(this);
        this.start = this.start.bind(this);
        this.space = this.space.bind(this);
    }
    space(index) {
        const n = index + '';
        return (
            n +
            Array.from({ length: 10 - n.length })
                .map(() => ' ')
                .join('')
        );
    }
    write(resolve) {
        while (this.writeable) {
            console.log('rest:', this.rest);
            this.writeable = this.stream.write(
                `${this.space(this.total - this.rest)}${this.text}`,
            );
            if (this.writeable) {
                this.rest -= 1;
                if (!this.rest) {
                    this.stream.destroy();
                    this.writeable = false;
                    resolve();
                }
            } else {
                this.stream.once('drain', () => {
                    setTimeout(() => {
                        console.log('开始续写:');
                        this.writeable = true;
                        this.write(resolve);
                    });
                });
            }
        }
    }
    start() {
        return new Promise((resolve, reject) => {
            try {
                this.stream.once('error', (error) => {
                    this.writeable = false;
                    this.stream.destroy();
                    reject(error);
                });
                this.write(resolve, reject);
            } catch (reject) {
                reject(reject);
            }
        });
    }
}

async function cretaeBigText() {
    const hashBigText = await p_fs
        .stat(BigTextPath)
        .then(() => true)
        .catch(() => false);
    if (hashBigText) return;
    const statStopSteam = new StatStopTream({ total: 1e8, taskLength: 30000 });
    return statStopSteam.start();
}

/**
 * @name:
 * @description:
 * @param {fs.WriteStream} dest
 * @param {()=>string|Buffer} chunk
 * @param {{value?:NodeJS.Timeout}} timer
 */
function witeHugeFileWithStream(dest, chunk, timer) {
    return new Promise((resolve, reject) => {
        let writeable = true;
        let i = 0;
        dest.once('error', (error) => {
            writeable = false;
            dest.end();
            console.log('reject:');
            reject(error);
        });
        function write() {
            while (writeable) {
                const content = chunk();
                console.log('content:', !!content, ++i);
                if (content == undefined) {
                    writeable = false;
                    dest.end();
                    console.log('resolve:');
                    resolve();
                    return;
                }
                writeable = dest.write(content, 'utf-8');
                if (!writeable) {
                    dest.once('drain', () => {
                        timer && (timer.value = setTimeout(write));
                    });
                    return;
                }
            }
        }
        write();
    });
}
(async () => {
    try {
        const dest = fs.createWriteStream(BigTextPath2, { flags: 'ax' });
        let total = 1000;
        const text = () => {
            --total;
            console.log('total:', total);
            if (total === -1) return;
            return (
                total +
                ' Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n'
            );
        };
        const timer = {};
        await witeHugeFileWithStream(dest, text, timer).catch((error) => {
            console.log('error:', error);
        });
        console.log('写入成功:');
    } catch (error) {
        console.log('error:', error);
    }
})();

module.exports = {
    cretaeBigText,
    BigTextPath,
    CWD,
};
