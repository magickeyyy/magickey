require('colors');
const esbuild = require('esbuild');
const { EventEmitter } = require('events');
const nodemon = require('nodemon');
const path = require('path');

async function start() {
    try {
        const emitter = new EventEmitter();
        const ctx = await esbuild.context({
            bundle: true,
            outdir: './dist',
            entryPoints: ['./src/index.js'],
            platform: 'node',
            sourcemap: 'linked',
            target: 'node16',
            loader: {
                '.png': 'dataurl',
            },
            plugins: [
                {
                    name: 'build-finished',
                    setup({ onEnd }) {
                        onEnd(() => {
                            emitter.emit('build-finished');
                        });
                    },
                },
            ],
        });
        // rebuild
        emitter.on('build-finished', () => {});
        await ctx.watch();
        console.log('启动构建程序成功，持续监控中...'.blue);
        const watcher = nodemon({
            script: path.join(process.cwd(), 'dist/index.js'),
            watch: [path.join(process.cwd(), 'dist')],
            quiet: true,
        });
        watcher.on('log', (msg) => {
            console.log(msg.message);
        });
    } catch (error) {
        console.log('启动构建程序失败:', error);
        process.exit(1);
    }
}
function build() {
    return esbuild
        .build({
            bundle: true,
            outdir: './dist',
            entryPoints: ['./src/index.js'],
            platform: 'node',
            sourcemap: 'linked',
            target: 'node16',
            loader: {
                '.png': 'dataurl',
            },
        })
        .then(() => {
            console.log('构建成功'.green);
        })
        .catch(() => {
            console.log('构建失败'.red);
            console.log(error);
        });
}

function launch() {
    process.argv.includes('--start') ? start() : build();
}
launch();
