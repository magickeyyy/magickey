import path from 'node:path';
import { createRequire } from 'node:module';
import { rollup, watch } from 'rollup';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import inputAlias from '../plugins/input-alias/index.mjs';
import json from '@rollup/plugin-json';

const require = createRequire(import.meta.url);
const { loadConfigFile } = require('rollup/loadConfigFile');

const IsDev = process.env.NODE_ENV === 'development';

async function build() {
    const { options, warnings } = await loadConfigFile(
        path.resolve(process.cwd(), 'rollup.config.mjs'),
    );
    const [bundle] = await Promise.all([
        rollup({
            input: 'src/index.js',
            plugins: [
                inputAlias({ alias: { '@': './src' } }),
                commonjs({
                    strictRequires: true,
                    extensions: ['.js', '.mjs'],
                    sourceMap: true,
                }),
                nodeResolve(),
                json(),
            ],
        }),
        watch({
            input: 'src/index.js',
            output: {
                file: 'dist/index.js',
                format: 'cjs',
                sourcemap: true,
            },
            plugins: [
                inputAlias({ alias: { '@': './src' } }),
                commonjs({
                    strictRequires: true,
                    extensions: ['.js', '.mjs'],
                    sourceMap: true,
                }),
                nodeResolve(),
                json(),
            ],
            watch: {
                buildDelay: 800,
                clearScreen: true,
                include: 'src/**',
            },
        }),
    ]);
    await bundle.write({
        file: 'dist/index.js',
        format: 'cjs',
        sourcemap: true,
    });
    await bundle.close();
}

build();

export default build;
