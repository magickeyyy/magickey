import fs from 'fs';
import path from 'path';

const CWD = process.cwd();
const BigTextPath2 = path.join(CWD, 'dustbin', '.big_text2.text');

/**
 * @name 用流写一个文件
 * @description 处理了在写入大文件时内存溢出问题。
 * @description 会阻塞I/O
 * @param {fs.WriteStream} dest
 * @param {()=>string|Buffer} chunk
 */
function writeHugeFileWithStream(dest, chunk) {
    return new Promise((resolve, reject) => {
        let writeable = true;
        dest.once('error', (error) => {
            writeable = false;
            dest.end();
            reject(error);
        });
        function write() {
            while (writeable) {
                const content = chunk();
                if (content == undefined) {
                    writeable = false;
                    dest.end();
                    resolve();
                    return;
                }
                writeable = dest.write(content, 'utf-8');
                if (!writeable) {
                    dest.once('drain', () => {
                        process.nextTick(() => {
                            writeable = true;
                            write();
                        });
                    });
                    return;
                }
            }
        }
        write();
    });
}

async function bigText2() {
    const dirname = path.dirname(BigTextPath2);
    const mkdir = () =>
        fs.promises
            .mkdir(dirname)
            .then(() => true)
            .catch(() => false);
    const hasDir = await fs.promises
        .stat(dirname)
        .then((stat) => (stat.isDirectory() ? true : mkdir()))
        .catch(mkdir);
    if (!hasDir) return;
    const noText = await fs.promises
        .stat(BigTextPath2)
        .then((stat) =>
            stat.isFile()
                ? fs.promises
                      .rm(BigTextPath2)
                      .then(() => true)
                      .catch(() => false)
                : true,
        )
        .catch(() => true);
    if (!noText) return;
    let total = 1e9;
    const text = () => {
        --total;
        console.log('total:', total);
        if (total === -1) return;
        return (
            total +
            ' Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n'
        );
    };
    const dest = fs.createWriteStream(BigTextPath2, { flags: 'ax' });
    await writeHugeFileWithStream(dest, text)
        .then(() => {
            console.log('写入成功:');
        })
        .catch((error) => {
            console.log('写入失败:', error);
        });
}

export { writeHugeFileWithStream, bigText2 };
