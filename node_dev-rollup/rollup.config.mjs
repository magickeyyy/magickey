import { nodeResolve } from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import inputAlias from 'rollup-plugin-input-alias';
import json from '@rollup/plugin-json';

const IsDev = process.env.NODE_ENV === 'development';

/**
 * @name:
 * @description rollup配置可以是一个配置，也可以是多个配置（数组）。输出可以是一个配置，也可以是多个配置（数组）
 */
const config = {
    input: 'src/index.js',
    output: {
        file: 'dist/index.js',
        format: 'cjs',
        sourcemap: true,
    },
    plugins: [
        inputAlias({ alias: { '@': './src', $: '.' } }),
        nodeResolve(),
        commonjs({
            strictRequires: true,
            extensions: ['.js', '.mjs'],
            sourceMap: true,
        }),
        json(),
    ],
};
if (IsDev) {
    config.watch = { buildDelay: 800, clearScreen: true, include: 'src/**' };
}

export default config;
