import path from 'path';
import fs from 'fs';
import { isRecord, isString, findFile } from './utils.mjs';

const PluginName = 'input-alias';
const NullValue = { name: PluginName, resolveId: () => null };

/**
 * @name 路径别名
 * @description 别名只能映射到目录，不能隐射到文件
 * @description 映射的路径必须是与cwd的相对路径
 * @param {{alias:Record<string,string>,cwd?:string}} options 别名隐射关系，值是基于工作目录的相对路径
 */
export default async function inputAlias(options) {
    if (
        !isRecord(options) ||
        !isRecord(options.alias) ||
        !Object.keys(options.alias).length
    ) {
        console.warn('incorrect options');
        return NullValue;
    }
    const cwd = options.cwd ?? process.cwd();
    const aliases = await Promise.all(
        Object.entries(options.alias).map(([alias, value]) =>
            fs.promises.lstat(path.join(cwd, value)).then((stat) => {
                if (stat.isDirectory() || stat.isSymbolicLink()) {
                    return alias;
                }
                throw new Error(`${alias} is not a directory or symbolic link`);
            }),
        ),
    );
    /**
     * @description 找出别名映射的路径
     */
    const findAlias = (source) =>
        aliases.find((alias) => isString(source) && source.startsWith(alias));

    return {
        name: PluginName,
        async resolveId(source, importer, option) {
            const alias = findAlias(source);
            if (
                option.isEntry ||
                path.isAbsolute(source) ||
                !alias ||
                !importer
            )
                return this.resolve(
                    source,
                    importer,
                    Object.assign({ skipSelf: true }, option),
                ).then((resolved) => resolved || { id: source });
            const origin = await findFile(
                cwd,
                source.replace(alias, options.alias[alias]),
            );
            const to = path.resolve(cwd, origin);
            const from = path.isAbsolute(importer)
                ? importer
                : path.resolve(cwd, importer);
            let updatedSource = path
                .relative(path.dirname(from), to)
                .split(path.sep)
                .join('/');
            // 必须改成相对路径
            if (!updatedSource.startsWith('./')) {
                updatedSource = './' + updatedSource;
            }
            return this.resolve(
                updatedSource,
                importer,
                Object.assign({ skipSelf: true }, option),
            ).then((resolved) => resolved || { id: updatedSource });
        },
    };
}
