import path from 'path';
import fs from 'fs';

/**
 * @name 是否是对象
 * @param {any} data
 * @return {data is Record<PropertyKey,any>}
 */
function isRecord(data) {
    return Object.prototype.toString.call(data) === '[object Object]';
}

/**
 * @name 是否是string
 * @param {any} data
 * @return {data is string}
 */
function isString(data) {
    return typeof data === 'string';
}

const extensions = ['.js', '.ts'];

/**
 * @name 按规则找出缺省文件名的路径对应的文件路径
 * @description 如果路径已经指向一个文件，直接返回路径。
 * @description 否则认为路径指向一个文件夹，将按extensions的顺序一直解析到目标文件夹下index.xxx文件，否则报错
 * @param {string} cwd 工作目录路径
 * @param {string} src 基于工作目录的相对路径
 * @returns {string} 返回的是原src或者补全了index.xxx的src
 */
function findFile(cwd, src) {
    src = /\/$/.test(src) ? src.replace(/\/$/, '') : src;
    let ext = path.extname(src);
    let i = -1;
    return new Promise(async (success, reject) => {
        function off() {
            ++i;
            if (!extensions[i])
                return reject(
                    new Error('can not resolve ' + src + ' in ' + cwd),
                );
            ext = extensions[i];
            return recursion(src + '/index' + extensions[i]);
        }
        async function recursion(p) {
            if (ext) {
                const isFile = await fs.promises
                    .stat(path.resolve(cwd, p))
                    .then((stat) => stat.isFile())
                    .catch(() => false);
                if (isFile) return success(p);
                return off();
            }
            return off();
        }
        return recursion(src).catch(reject);
    });
}

export { isRecord, isString, findFile };
