import { useMemo, memo, useEffect } from 'react';
import { createPortal } from 'react-dom';
import { fixContainerHeight, uuid } from '@/utils';

export type RootPortalPropsType = React.PropsWithChildren<{
    /**
     * @name 插入dom容器
     * @default document.body
     * @description 只会使用一次
     */
    getContainer?: () => HTMLElement;
    portalKey?: string | null;
    /**
     * @description 最开始弹窗是不是要显示
     */
    display?: 'none' | '';
    className: string;
}>;

function RootPortal({
    getContainer = () => document.body,
    portalKey,
    display = 'none',
    className,
    children,
}: RootPortalPropsType) {
    const wrapper = useMemo(() => {
        const div = document.createElement('div');
        const id = uuid();
        div.id = id;
        div.className = className;
        getContainer()?.appendChild(div);
        div.style.display = display;
        return div;
    }, []);
    useEffect(() => {
        if (wrapper) {
            wrapper.style.display = display;
            fixContainerHeight(getContainer(), display === '');
        }
    }, [display]);
    return createPortal(children, wrapper, portalKey);
}

export default memo(RootPortal);
