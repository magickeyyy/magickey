import React, { useCallback, useEffect, useRef } from 'react';
import { animated, useSpring, to } from '@react-spring/web';
import classNames from 'classnames';
import type { ModalContentPropsType, ContentAnimationProperty } from './types';
import { useGetProp } from '../../hooks';
import { InitScale, InitOpacity } from './utils';

export default function ModalContent({
    centered = true,
    visible = false,
    maskClosable = true,
    duration,
    zIndex,
    onAnimationChange,
    onMaskClick,
    transform = (scale) => `scale(${scale})`,
    wrapperClassName,
    wrapperStyle,
    containerClassName,
    containerStyle,
    contentClassName,
    contentStyle,
    children,
}: ModalContentPropsType) {
    const wrapper = useRef<HTMLDivElement>(null);
    const container = useRef<HTMLDivElement>(null);
    const content = useRef<HTMLDivElement>(null);
    /**
     * 动画ID，动画时异步的，可能会交叉造成回调异常
     */
    const motionId = useRef({});
    const getDuration = useGetProp(duration);
    const getMaskClosable = useGetProp(maskClosable);
    const getOnAnimationChange = useGetProp(onAnimationChange);
    const getOnMaskClick = useGetProp(onMaskClick);
    const [{ opacity, scale }, api] = useSpring<ContentAnimationProperty>(() => ({
        from: { opacity: InitOpacity, scale: InitScale },
        to: { opacity: 1, scale: 1 },
        config: { duration },
    }));
    useEffect(() => {
        motion(visible);
    }, [visible]);
    const motion = useCallback(async (visible: boolean) => {
        const id = {};
        motionId.current = id;
        getOnAnimationChange()?.(visible ? 'open-start' : 'close-start');
        const [controller] = api.resume().start({
            from: {
                opacity: InitOpacity,
                scale: InitScale,
            },
            to: { opacity: 1, scale: 1 },
            reverse: !visible,
            config: {
                duration: getDuration(),
            },
        });
        await controller.then();
        if (motionId.current === id) {
            getOnAnimationChange()?.(visible ? 'open-end' : 'close-end');
        }
    }, []);
    const clickMask = useCallback((e: React.MouseEvent<HTMLDivElement>) => {
        if (!getMaskClosable()) return;
        if (e.target === wrapper.current || e.target === container.current) {
            getOnMaskClick()?.();
        }
    }, []);
    return (
        <div
            ref={wrapper}
            className={classNames('utm-modal-wrapper', wrapperClassName)}
            style={{ ...wrapperStyle, zIndex }}
            onClick={clickMask}
        >
            <div
                ref={container}
                className={classNames(
                    'utm-modal-container',
                    centered ? 'utm-modal-content_center' : undefined,
                    containerClassName,
                )}
                style={containerStyle}
            >
                <animated.div
                    ref={content}
                    className={classNames('utm-modal-content', contentClassName)}
                    style={{
                        opacity: opacity,
                        transform: to([scale], transform),
                        ...contentStyle,
                    }}
                >
                    {children}
                </animated.div>
            </div>
        </div>
    );
}
