import { isEffectiveNumber } from '@/utils';
export const InitScale = 0.1;
export const InitOpacity = 0.1;

export function calcModalTransformPos(content: HTMLDivElement, pos: { x: number; y: number }) {
    let { width, height } = content.getBoundingClientRect();
    width *= InitScale;
    height *= InitScale;
    const { x, y } = pos;
    if (!isEffectiveNumber(x) || !isEffectiveNumber(y)) return { x: 0, y: 0 };
    const { clientWidth: w, clientHeight: h } = document.body;
    // console.log(
    //     '%cssss:',
    //     'background-color:green;color:#fff;padding:2px 4px;border-radius:2px;',
    //     x,
    //     y,
    //     width,
    //     height,
    // );
    return { x: -(x - width / 2), y: -(y - height / 2) };
}
