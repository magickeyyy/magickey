/*
 * 默认渲染流程：当第一次visible
 * 默认动画时长0.25s，也是react-spring的默认动画时长
 */
import React, { memo, useEffect, useState, useCallback } from 'react';
import { useGetSet } from 'react-use';
import RootPortal from '@/components/RootPortal';
import ModalContent from './Content';
import Mask from '@/components/Mask';
import type { ModalPropsType, AnimationStatus } from './types';
import { useGetProp } from '@/hooks';

function Modal({
    mask = true,
    visible = false,
    forceRender = true,
    destroyAfterClose = true,
    duration = 250,
    zIndex = 1000,
    maskContent,
    maskClassName,
    maskStyle,
    portalKey,
    getContainer,
    onMaskClick,
    onVisibleChange,
    onAnimationChange,
    children,
    ...restProps
}: ModalPropsType) {
    const getOnVisibleChange = useGetProp(onVisibleChange);
    const getOnAnimationChange = useGetProp(onAnimationChange);
    /**
     * visible是否为true过，或者forceRender=true
     */
    const [getMount, setMount] = useGetSet(() => {
        if (forceRender && visible) return true;
        return visible === true;
    });
    useEffect(() => {
        if (!getMount() && visible) {
            setMount(true);
        }
        getOnVisibleChange()?.(!!visible);
    }, [visible]);
    /**
     * @description 动画结束后根元素的display
     */
    const [display, setDisplay] = useState<'none' | ''>(
        forceRender && visible ? '' : visible ? '' : 'none',
    );
    const animationStatus = useCallback((status: AnimationStatus) => {
        getOnAnimationChange()?.(status);
        if (status === 'open-start') {
            setDisplay('');
        } else if (status === 'close-end') {
            setDisplay('none');
        }
    }, []);
    return (
        <RootPortal
            getContainer={getContainer}
            portalKey={portalKey}
            display={display}
            className="utm-modal-root"
        >
            {getMount() ? (
                <>
                    {mask ? (
                        <Mask
                            style={maskStyle}
                            className={maskClassName}
                            duration={duration}
                            visible={visible}
                            zIndex={zIndex}
                        >
                            {maskContent}
                        </Mask>
                    ) : null}
                    <ModalContent
                        duration={duration}
                        visible={visible}
                        onAnimationChange={animationStatus}
                        onMaskClick={onMaskClick}
                        zIndex={zIndex}
                        {...restProps}
                    >
                        {children}
                    </ModalContent>
                </>
            ) : null}
        </RootPortal>
    );
}
export default memo(Modal);
