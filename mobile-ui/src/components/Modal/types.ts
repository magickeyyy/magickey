import type { RootPortalPropsType } from '../RootPortal';

type ClickPosRef = { x?: number; y?: number; effective: boolean };
type AnimationStatus = 'open-start' | 'open-end' | 'close-start' | 'close-end';
type UsefulNode =
    | string
    | number
    | React.ReactElement<any, string | React.JSXElementConstructor<any>>
    | React.ReactFragment
    | React.ReactPortal;
type OnAnimationChange = (status: AnimationStatus) => void;
type ModalMaskPropsType = React.PropsWithChildren<{
    visible?: boolean;
    duration?: number;
    style?: React.CSSProperties;
    className?: string;
}>;
type ModalContentPropsType = React.PropsWithChildren<{
    /**
     * @name 开关
     */
    visible?: boolean;
    /**
     * @dname 动画时长
     */
    duration?: number;
    /**
     * @name 动画阶段回调
     * @description 动画执行到不同阶段的回调
     */
    onAnimationChange?: OnAnimationChange;
    /**
     * @name 点击蒙层是否关闭modal
     * @default false
     * @description: false 也会禁用onMaskClick
     * @return {*}
     */
    maskClosable?: boolean;
    /**
     * @name 遮罩点击事件
     * @description 点击的实际上是content容器，在遮罩之上
     */
    onMaskClick?: () => void;
    /**
     * @name transform样式
     * @description
     */
    transform?: (scale: number) => string;
    maskClassName?: string;
    maskStyle?: React.CSSProperties;
    wrapperClassName?: string;
    wrapperStyle?: React.CSSProperties;
    containerClassName?: string;
    containerStyle?: React.CSSProperties;
    contentClassName?: string;
    contentStyle?: React.CSSProperties;
    /**
     * @name 水平垂直居中
     * @default true
     */
    centered?: boolean;
    zIndex?: React.CSSProperties['zIndex'];
}>;
type MaskAnimationProperty = { opacity: number };
type ContentAnimationProperty = { opacity: number; scale: number };
type ModalPropsType = {
    /**
     * @name 是否显示遮罩
     */
    mask?: boolean;
    /**
     * @name 是否显示对话框
     */
    visible?: boolean;
    /**
     * @name 强制渲染子组件
     * * 默认在第一次visible为true时才渲染modal内容。为true时不管visible都渲染内容。
     * * 只取组件首次加载时的值，动态变更无效（可以再包一层）
     * @default true
     */
    forceRender?: boolean;
    /**
     * @name 卸载后销毁子组件
     * @description 卸载后会卸载portal和子组件
     * @default true
     */
    destroyAfterClose?: boolean;
    /**
     * 遮罩内容
     */
    maskContent?: React.ReactNode;
    /**
     * * 打开和关闭的动画时间，单位ms，默认250
     * * 一个值就是开始结束都一个时间，两个值就是[开始，结束]
     * @unit ms
     * @default 250
     */
    duration?: number;
    /**
     * 点击遮罩事件，动画期间不会执行
     */
    onMaskClick?: ModalContentPropsType['onMaskClick'];
    /**
     * 开关变量变化回调
     */
    onVisibleChange?: (visible: boolean) => void;
    /**
     * * 动画执行到不动阶段的钩子：open-start打开动画开始；open-end打开动画结束；close-start关闭动画开始；close-end关闭动画结束
     * * 可以在不同阶段处理容器的滚动条（处理滚动不是简单的容器的overflow）
     */
    onAnimationChange?: OnAnimationChange;
    /**
     * 对话框层级
     */
    zIndex?: React.CSSProperties['zIndex'];
    /**
     * 当对话框关闭动画完成后是否卸载子元素
     * @default false
     */
    destroyChildrenAfterClose?: boolean;
    /**
     * 对话框容器元素类名
     */
    wrapperClassName?: string;
    /**
     * 对话框容器元素样式
     */
    wrapperStyle?: React.CSSProperties;
    /**
     * 对话框内容容器元素类名
     */
    contentClassName?: string;
    /**
     *对话框内容容器元素样式
     */
    contentStyle?: React.CSSProperties;
} & Omit<ModalContentPropsType, 'visible'> &
    Omit<ModalMaskPropsType, 'key' | 'onClick' | 'visible'> &
    Omit<RootPortalPropsType, 'className'>;

export type {
    AnimationStatus,
    OnAnimationChange,
    ModalMaskPropsType,
    MaskAnimationProperty,
    ModalPropsType,
    ModalContentPropsType,
    ContentAnimationProperty,
    UsefulNode,
    ClickPosRef,
};
