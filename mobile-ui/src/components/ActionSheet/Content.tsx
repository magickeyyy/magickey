import React, { useCallback, useEffect, useRef } from 'react';
import { animated, useSpring, to } from '@react-spring/web';
import classNames from 'classnames';
import { useGetProp } from '@/hooks';
import {
    ActionContentPropsType,
    ActionSheetAnimationProperty,
    defActionSpringStyle,
} from './types';
import { useGetSet } from 'react-use';

export default function ModalContent({
    children,
    onAnimationChange,
    duration,
    visible = false,
    springStyle = defActionSpringStyle,
    contentClassName,
    contentStyle,
}: ActionContentPropsType) {
    const getDuration = useGetProp(duration);
    const getOnAnimationChange = useGetProp(onAnimationChange);
    const [getn, setn] = useGetSet(true);
    const content = useRef<HTMLDivElement>(null);
    /**
     * 动画ID，动画时异步的，可能会交叉造成回调异常
     */
    const motionId = useRef({});
    const [{ x }, api] = useSpring<ActionSheetAnimationProperty>(() => ({
        from: {
            x: 0,
        },
        to: { x: 1 },
    }));
    useEffect(() => {
        motion(visible);
    }, [visible]);
    const motion = useCallback(async (visible: boolean) => {
        const id = {};
        motionId.current = id;
        getOnAnimationChange()?.(visible ? 'open-start' : 'close-start');
        const [controller] = api.resume().start({
            from: {
                x: 0,
            },
            to: { x: 1 },
            reverse: !visible,
            immediate: getn(),
            config: {
                duration: getDuration(),
            },
        });
        await controller.then();
        getn() && setn(false);
        if (motionId.current === id) {
            getOnAnimationChange()?.(visible ? 'open-end' : 'close-end');
        }
    }, []);

    return (
        <animated.div
            ref={content}
            className={classNames('utm-action_sheet-content', contentClassName)}
            style={{
                ...contentStyle,
                transform: to([x], (x) => `translateY(${(1 - x) * 100}%)`),
            }}
        >
            {children}
        </animated.div>
    );
}
