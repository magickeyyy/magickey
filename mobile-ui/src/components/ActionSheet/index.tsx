import React, { memo, useState, useEffect, useCallback } from 'react';
import { useGetSet } from 'react-use';
import RootPortal from '@/components/RootPortal';
import Mask from '@/components/Mask';
import { useGetProp } from '@/hooks';
import type { ActionSheetPropsType } from './types';
import { AnimationStatus } from '@/components/Modal/types';
import Content from './Content';
import './index.scss';

function ActionSheet({
    getContainer,
    portalKey,
    visible = false,
    mask = true,
    forceRender = false,
    onAnimationChange,
    onVisibleChange,
    onMaskClick,
    children,
    duration,
    maskClassName,
    maskStyle,
}: ActionSheetPropsType) {
    const getOnVisibleChange = useGetProp(onVisibleChange);
    const getOnAnimationChange = useGetProp(onAnimationChange);
    /**
     * visible是否为true过，或者forceRender=true
     */
    const [getMount, setMount] = useGetSet(() => {
        if (forceRender && visible) return true;
        return visible === true;
    });
    useEffect(() => {
        if (!getMount() && visible) {
            setMount(true);
        }
        getOnVisibleChange()?.(!!visible);
    }, [visible]);
    /**
     * @description 动画结束后根元素的display
     */
    const [display, setDisplay] = useState<'none' | ''>(
        forceRender && visible ? '' : visible ? '' : 'none',
    );
    const animationStatus = useCallback((status: AnimationStatus) => {
        console.log('status:', status);
        getOnAnimationChange()?.(status);
        if (status === 'open-start') {
            setDisplay('');
        } else if (status === 'close-end') {
            setDisplay('none');
        }
    }, []);
    return (
        <RootPortal
            getContainer={getContainer}
            portalKey={portalKey}
            display={display}
            className="utm-action_sheet-root"
        >
            {mask ? (
                <Mask
                    visible={visible}
                    duration={duration}
                    style={maskStyle}
                    className={maskClassName}
                    onClick={onMaskClick}
                />
            ) : null}
            <Content
                onAnimationChange={animationStatus}
                visible={visible}
                onMaskClick={onMaskClick}
                duration={duration}
            >
                {children}
            </Content>
        </RootPortal>
    );
}

export default memo(ActionSheet);
