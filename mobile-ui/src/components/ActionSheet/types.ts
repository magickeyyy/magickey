import type { RootPortalPropsType } from '@/components/RootPortal';
import type { MaskPropsType } from '@/components/Mask/types';
import type { OnAnimationChange } from '@/components/Modal/types';
import type { Interpolator, SpringValue } from '@react-spring/web';

type ActionSheetPropsType = {
    /**
     * @name 遮罩
     * @description 是否生成遮罩
     */
    mask?: boolean;
    visible?: boolean;
    forceRender?: boolean;
    maskClassName?: string;
    maskStyle?: React.CSSProperties;
    /**
     * @name 水平垂直居中
     * @default true
     */
    centered?: boolean;
    duration?: number;
    zIndex?: React.CSSProperties['zIndex'];
} & ActionContentPropsType &
    Omit<RootPortalPropsType, 'className'>;

type ActionContentPropsType = React.PropsWithChildren<{
    visible?: boolean;
    onMaskClick?: () => void;
    /**
     * @dname 动画时长
     */
    duration?: number;
    /**
     * @name 动画阶段回调
     * @description 动画执行到不同阶段的回调
     */
    onAnimationChange?: OnAnimationChange;
    onVisibleChange?: (visible: boolean) => void;
    /**
     * @name 点击蒙层是否关闭modal
     * @default false
     * @description: false 也会禁用onMaskClick
     * @return {*}
     */
    maskClosable?: boolean;
    /**
     * @name transform样式
     * @description
     */
    transform?: (scale: number) => string;
    wrapperClassName?: string;
    wrapperStyle?: React.CSSProperties;
    containerClassName?: string;
    containerStyle?: React.CSSProperties;
    contentClassName?: string;
    contentStyle?: React.CSSProperties;
    springStyle?: (
        x: SpringValue<number>,
    ) => React.CSSProperties & Record<keyof React.CSSProperties, ReturnType<Interpolator>>;
}>;
type ActionSheetAnimationProperty = { x: number };

function defActionSpringStyle(x: SpringValue<number>) {
    return x ? ({ transform: x.to((x) => `-${(1 - x) * 100}%`) } as any) : {};
}

export { defActionSpringStyle };
export type { ActionSheetPropsType, ActionContentPropsType, ActionSheetAnimationProperty };
