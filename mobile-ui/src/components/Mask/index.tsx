import React, { useRef, useEffect, useCallback } from 'react';
import { animated, useSpring, to } from '@react-spring/web';
import type { MaskPropsType, MaskAnimationProperty } from './types';
import './index.scss';
import { useGetProp } from '@/hooks';
import classNames from 'classnames';
import { useGetSet } from 'react-use';

export default function Mask({
    visible = false,
    duration,
    zIndex,
    style,
    className,
    onClick,
    children,
}: MaskPropsType) {
    const div = useRef<HTMLDivElement>(null);
    const getDuration = useGetProp(duration);
    const [getn, setn] = useGetSet(true);
    useEffect(() => {
        motion(visible);
    }, [visible]);
    const [{ opacity }, api] = useSpring<MaskAnimationProperty>(() => ({
        from: { opacity: 0 },
        to: { opacity: 1 },
    }));
    const motion = useCallback(async (visible: boolean) => {
        const [controller] = api.resume().start({
            from: { opacity: 0 },
            to: { opacity: 1 },
            reverse: !visible,
            immediate: getn(),
            config: { duration: getDuration() },
        });
        await controller.then();
        getn() && setn(false);
    }, []);
    return (
        <animated.div
            ref={div}
            className={classNames('utm-mask', className)}
            style={{ ...style, opacity: to([opacity], (x) => x), zIndex }}
            onClick={onClick}
        >
            {children}
        </animated.div>
    );
}
