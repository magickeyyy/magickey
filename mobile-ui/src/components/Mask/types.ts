type MaskPropsType = React.PropsWithChildren<{
    visible?: boolean;
    duration?: number;
    style?: React.CSSProperties;
    className?: string;
    onClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
    zIndex?: React.CSSProperties['zIndex'];
}>;

type MaskAnimationProperty = { opacity: number };

export type { MaskPropsType, MaskAnimationProperty };
