import React from 'react';
import { Link } from 'react-router-dom';

export default function RootPage() {
    return (
        <ul>
            <li>
                <Link to="/modal">Modal</Link>
            </li>
            <li>
                <Link to="/actionSheet">ActionSheet</Link>
            </li>
        </ul>
    );
}
