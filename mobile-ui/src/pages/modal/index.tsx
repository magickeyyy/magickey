import React, { useState } from 'react';
import Modal from '@/components/Modal';

export default function ModalPage() {
    const [visible, setVisible] = useState(false);
    const onClick = () => {
        setVisible((pre) => !pre);
    };
    const onMaskClick = () => {
        setVisible(false);
    };
    return (
        <div>
            <button style={{ width: '60px', height: '30px' }} onClick={onClick}>
                {visible ? '关闭' : '打开'}
            </button>
            <Modal visible={visible} forceRender={false} onMaskClick={onMaskClick}>
                <p>
                    1.Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae alias officiis
                    praesentium sed consequatur error nihil impedit laudantium sit. Velit fugiat
                    laboriosam molestiae natus. Laudantium eius sapiente blanditiis iste deleniti.
                </p>
                <p>
                    2.Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum a soluta neque
                    perferendis assumenda alias, sit rem, dolorum tenetur recusandae esse ipsam
                    voluptatem dolorem aperiam quis exercitationem facere modi hic?
                </p>
                <p>
                    3.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptatibus
                    repellat harum sed perferendis deserunt, vel ex. Reiciendis amet natus aut fuga
                    fugit, tempora dolor sit quam, odio quisquam porro voluptas.
                </p>
                <p>
                    4.Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nemo exercitationem
                    error ut qui voluptatibus? Eius minima voluptatum aliquam repellat laborum eos
                    earum distinctio sint culpa, fugiat iure, saepe ea esse?
                </p>
            </Modal>
        </div>
    );
}
