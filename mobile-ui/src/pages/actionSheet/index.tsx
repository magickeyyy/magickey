import React, { useState } from 'react';
import ActionSheet from '@/components/ActionSheet';

export default function ActionSheetPage() {
    const [visible, setVisible] = useState(false);
    const onClick = () => {
        setVisible((pre) => !pre);
    };
    const onMaskClick = () => {
        setVisible(false);
    };
    return (
        <div>
            <button style={{ width: '60px', height: '30px' }} onClick={onClick}>
                {visible ? '关闭' : '打开'}
            </button>
            <ActionSheet visible={visible} onMaskClick={onMaskClick}>
                <p>
                    1.Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae alias officiis
                    praesentium sed consequatur error nihil impedit laudantium sit. Velit fugiat
                    laboriosam molestiae natus. Laudantium eius sapiente blanditiis iste deleniti.
                </p>
                <p>
                    2.Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum a soluta neque
                    perferendis assumenda alias, sit rem, dolorum tenetur recusandae esse ipsam
                    voluptatem dolorem aperiam quis exercitationem facere modi hic?
                </p>
                {/* <p>
                    3.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptatibus
                    repellat harum sed perferendis deserunt, vel ex. Reiciendis amet natus aut fuga
                    fugit, tempora dolor sit quam, odio quisquam porro voluptas.
                </p>
                <p>
                    4.Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nemo exercitationem
                    error ut qui voluptatibus? Eius minima voluptatum aliquam repellat laborum eos
                    earum distinctio sint culpa, fugiat iure, saepe ea esse?
                </p>
                <p>
                    5.Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates omnis
                    voluptatibus sint nisi aut necessitatibus, sapiente voluptatem? Veritatis,
                    dolorum commodi architecto corrupti iure aut, nulla doloremque, earum possimus
                    quidem mollitia?
                </p>
                <p>
                    6.Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla commodi odit,
                    explicabo iste molestiae, non sunt optio cum temporibus ducimus eveniet!
                    Adipisci doloremque temporibus ratione asperiores distinctio impedit voluptate
                    expedita.
                </p>
                <p>
                    7.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptate quaerat
                    cumque fuga quam quo magnam voluptatibus, odio mollitia. Quisquam nostrum
                    consequatur dicta enim voluptatibus saepe in voluptatum voluptas porro cumque!
                </p>
                <p>
                    8.Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias totam
                    doloribus itaque rem magni pariatur ab laboriosam amet eaque porro. Eum
                    molestiae fuga natus velit officiis, repudiandae rem saepe culpa?
                </p> */}
                {/* <p>
                    9.Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facere, asperiores
                    eaque itaque explicabo quam eius nemo, debitis reiciendis inventore dignissimos
                    quibusdam quae illo ullam sint architecto accusantium quo possimus dolorum?
                </p>
                <p>
                    10.Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repudiandae vero
                    mollitia quasi ipsa quia, at inventore ipsam consectetur ad nemo iusto nulla
                    quae porro quod dignissimos sunt explicabo pariatur facere.
                </p>
                <p>
                    11.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eaque alias quod
                    odio, voluptatum modi laudantium ut illum, suscipit asperiores placeat sed
                    libero error hic perferendis totam nostrum eum possimus perspiciatis!
                </p>
                <p>
                    12.Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolorum magnam
                    expedita laudantium ipsam nemo ipsa veritatis officiis quo mollitia accusamus
                    optio ratione cumque est illo, corrupti non quod vitae natus!
                </p>
                <p>
                    13.Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa ullam maxime
                    harum, soluta, quod mollitia molestiae temporibus optio et eveniet quisquam
                    laboriosam, architecto voluptatem quam omnis voluptate illo! Illum, debitis?
                </p> */}
            </ActionSheet>
        </div>
    );
}
