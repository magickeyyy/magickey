import React from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import RootPage from '@/pages/root';
import ModalPage from '@/pages/modal';
import ActionSheetPage from './pages/actionSheet';

const router = createBrowserRouter([
    { path: '/', element: <RootPage /> },
    { path: '/modal', element: <ModalPage /> },
    { path: '/actionSheet', element: <ActionSheetPage /> },
]);

function App() {
    return <RouterProvider router={router} />;
}
export default App;
