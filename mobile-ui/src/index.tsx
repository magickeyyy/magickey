import ReactDOM from "react-dom/client";
import "./global.scss";
import App from "./App";

const root = ReactDOM.createRoot(
    document.getElementById("root") as HTMLElement
);

/**
 * ! 不要开启React.StrictMode，会开启实验性的并发渲染，组件会渲染两次，BUG一堆
 */
root.render(<App />);
