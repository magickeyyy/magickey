/**
 * @description: 生成uuid v4 字符串
 */
function uuid() {
    const url = URL.createObjectURL(new Blob()).split('/').pop() as string;
    URL.revokeObjectURL(url);
    return url;
}

/**
 * @description: 宏任务的nextTick
 */
function nextTick(callback?: () => void, timer?: { id?: NodeJS.Timeout }) {
    return new Promise<void>((resolve, reject) => {
        const id = setTimeout(() => {
            try {
                callback?.();
            } catch (error) {
                reject(error);
            }
            resolve();
        });
        if (timer) {
            timer.id = id;
        }
    });
}
function isNumber(data: unknown): data is number {
    return typeof data === 'number';
}
function isEffectiveNumber(data: unknown): data is number {
    return typeof data === 'number' && !Number.isNaN(data) && Number.isFinite(data);
}
function isStrictObject(data: unknown): data is Record<string, any> {
    return Object.prototype.toString.call(data) === '[object Object]';
}

/**
 * @name 固定容器的高度
 * @description: 在打开弹窗等组件时，固定容器的高度；关闭时撤销高度固定
 * @param {HTMLElement} container
 * @param {boolean} fix
 * @return {*}
 */
function fixContainerHeight(container: HTMLElement, fix: boolean) {
    const scrollTop = container.scrollTop;
    if (fix) {
        container.style.height = window.innerHeight + 'px';
        container.style.overflow = 'hidden';
        container.scrollTop = scrollTop;
    } else {
        container.scrollTop = scrollTop;
        container.style.height = '';
        container.style.overflow = '';
    }
}

export { uuid, nextTick, isNumber, isEffectiveNumber, isStrictObject, fixContainerHeight };
