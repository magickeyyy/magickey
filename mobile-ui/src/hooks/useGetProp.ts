import { useRef, useCallback } from 'react';

export default function useGetProp<T = any>(prop: T) {
    const ref = useRef(prop);
    ref.current = prop;
    return useCallback(() => ref.current, []);
}
