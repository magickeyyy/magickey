# mobile-ui

## 动画

&emsp;&emsp;动画全部使用 @react-spring/web 完成。

> 系需要计算一个动画参数（0-1），其余样式由该值计算所得。

## @react-spring/web

> 1.初始化时如果没传配置，x 也没有初始化

```js
const [{ x }, api] = useSpring(() => ({ from: { x: 0 }, to: { y: 0 } }));
```
