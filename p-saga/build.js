/*
 * 构建、生成其他npm package文件
 * build后到dist目录 npm login;npm publish
 */
const shell = require('shelljs');
const path = require('path');
const fs = require('fs/promises');

const version = process.argv[2];
if (
    version &&
    !(
        ['major', 'minor', 'patch'].includes(version) ||
        /^\d+\.\d+\.\d+/.test(version)
    )
) {
    console.log('version不合规范');
    process.exit(1);
}
function execAsync(command, options) {
    return new Promise((resolve, reject) => {
        shell.exec(command, options, (code, value, error) => {
            if (code !== 0) {
                return reject(error);
            }
            resolve(value);
        });
    });
}
/**
 * @name:
 * @description 修改readme coverage，拷贝到dist
 */
async function readme() {
    const statements = require('./coverage/coverage-summary.json').total
        .statements.pct;
    const md = await fs.readFile(path.resolve(__dirname, 'README.md'), 'utf-8');
    let bg = 'success';
    if (statements < 60) {
        bg = 'red';
    } else if (statements < 80) {
        bg = 'blue';
    }
    const text = md.replace(
        /coverage-\d{1,3}%25-(success|blue|red)/,
        `coverage-${statements}%25-${bg}`,
    );
    await fs.writeFile(path.resolve(__dirname, 'dist/README.md'), text);
}

/**
 * @name:
 * @description 重写package.json
 * @return {*}
 */
async function package() {
    const {
        name,
        version,
        description,
        keywords,
        author,
        license,
        repository,
    } = require('./package.json');
    return fs.writeFile(
        path.resolve(__dirname, 'dist/package.json'),
        JSON.stringify(
            {
                name,
                version,
                description,
                keywords,
                author,
                license,
                repository,
                main: './index.js',
                module: './index.mjs',
                typings: './index.d.ts',
            },
            undefined,
            2,
        ),
    );
}

(async () => {
    await Promise.all([
        fs
            .rm(path.resolve(__dirname, 'dist'), { recursive: true })
            .catch(() => {}),
        fs
            .rm(path.resolve(__dirname, 'coverage'), { recursive: true })
            .catch(() => {}),
    ]);
    await execAsync('npm run rollup');
    await execAsync('npm run test');
    if (version) {
        await execAsync(`npm version ${version}`);
    }
    await readme();
    await package();
})();
