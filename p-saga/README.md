# p-saga

基于 Promise 封装的常用处理异步任务的方法。

<div style="display:flex;justify-content:center;align-items:center;"><img src="https://img.shields.io/badge/license-MIT-brightgreen" />&emsp;<img src="https://img.shields.io/badge/test-passing-success" />&emsp;<img src="https://img.shields.io/badge/coverage-100%25-success" /></div>

## 方法

### series

逐个执行任务

### map

异步任务按顺序逐个执行，全部完成后返回全部结果。

### reduce

异步任务逐个执行，每个任务能查到已完成的任务，全部完成后返回全部结果。

### batch

按固定任务数量并发，逐批次完成全部任务，全部完成后返回全部结果。

### outburst

按照指定数量同时启动第一批，每个任务完成后启动一个剩余任务，直至全部完成返回全部结果。

## 使用

```ts
import { series, map, reduce, batch, outburst } from 'p-saga';

map(
    Array.from({ length: 20 }).map(
        () => (page: number) =>
            fetch(new Request(`/mock/list?page=${page}&size=20`), {
                method: 'GET',
            }),
    ),
);
```

### 全局使用

```ts
// 入口文件
import 'p-saga/bind';
```

```ts
// typings.d.ts

declare interface PromiseConstructor {
    series: <T = any>(tasks: ((index: number, copiedTasks: any[]) => T | Promise<T>)[]): Promise<void>;
    map: <T = any>(tasks: ((index: number, copiedTasks: any[]) => T | Promise<T>)[]): Promise<T[]>;
    reduce: <T>(tasks: ((previousValue: T[], index: number, copiedTasks: any[]) => Promise<T[]> | T[])[], initialValue?: T[]): Promise<T[]>;
    batch: <T = any>(count: number, tasks: ((index: number, batchIndex: number) => T | Promise<T>)[]): Promise<T[]>;
    outburst: <T = any>(count: number, tasks: Array<(index: number) => T | Promise<T>>): Promise<T[]>;
}
```
