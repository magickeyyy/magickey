const path = require('path');
const fs = require('fs/promises');
const { rollup } = require('rollup');
const typescript = require('rollup-plugin-typescript2');
const { terser } = require('rollup-plugin-terser');

async function build(names) {
    const bundles = await Promise.all(
        names.map((name) =>
            rollup({
                input: path.resolve(__dirname, `${name}.ts`),
                plugins: [
                    typescript({
                        tsconfigOverride: {
                            compilerOptions: { module: 'ESNext' },
                            exclude: [
                                './coverage',
                                './dist',
                                './rollup.config.js',
                                './jest.config.ts',
                                './index.test.ts',
                            ],
                        },
                    }),
                    terser(),
                ],
            }),
        ),
    );
    await fs
        .rm(path.resolve(__dirname, 'dist'), { recursive: true })
        .catch(() => {});
    await Promise.all(
        bundles.map((bundle, index) =>
            Promise.all([
                bundle.write({
                    format: 'commonjs',
                    file: path.resolve(__dirname, `dist/${names[index]}.js`),
                    sourcemap: true,
                }),
                bundle.write({
                    format: 'esm',
                    file: path.resolve(__dirname, `dist/${names[index]}.mjs`),
                    sourcemap: true,
                }),
            ]),
        ),
    );
}

build(['index', 'bind']);
