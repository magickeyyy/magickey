function runPromiseOrReturnRes<T = any>(
    promise: T | Promise<T>,
    resolve: (result: T) => void,
    reject: (error: any) => void,
) {
    if (promise instanceof Promise) {
        promise.then(resolve).catch(reject);
    } else {
        resolve(promise);
    }
}

function tryfn<T extends (...args: any[]) => any>(
    fn: T,
    ...args: Parameters<T>
) {
    try {
        return fn(...args);
    } catch (error) {
        return Promise.reject(error);
    }
}

function isFPIN(n?: any): n is number {
    return (
        typeof n === 'number' &&
        !Number.isNaN(n) &&
        n > 0 &&
        Math.floor(n) === n &&
        Number.isFinite(n)
    );
}

/**
 * @name 按序执行异步任务
 * @description 异步任务按顺序逐个执行，一个失败立即reject，全部完成后结束任务
 * @description 终止任务：tasks已复制，可以截断、清空，或者抛出异常
 */
export function series<T = any>(
    tasks: ((index: number, copiedTasks: any[]) => T | Promise<T>)[],
) {
    return new Promise<void>((resolve, reject) => {
        if (!Array.isArray(tasks))
            return reject(new Error('Expected `tasks` to be an array.'));
        if (!tasks.length) return resolve();
        let index = 0;
        tasks = tasks.slice();
        function success() {
            if (index + 1 >= tasks.length) {
                return resolve();
            }
            ++index;
            run();
        }
        function run() {
            const task = tryfn(tasks[index], index, tasks);
            runPromiseOrReturnRes(task, success, reject);
        }
        run();
    });
}

/**
 * @name 按序执行异步任务并返回全部结果
 * @description 异步任务按顺序逐个执行，一个失败立即reject，全部完成后返回全部结果
 * @description 终止任务：tasks已复制，可以截断、清空，或者抛出异常
 */
export function map<T = any>(
    tasks: ((index: number, copiedTasks: any[]) => T | Promise<T>)[],
) {
    return new Promise<T[]>((resolve, reject) => {
        const result: T[] = [];
        if (!Array.isArray(tasks))
            return reject(new Error('Expected `tasks` to be an array.'));
        if (!tasks.length) return resolve(result);
        let index = 0;
        tasks = tasks.slice();
        function success(res: T) {
            result.push(res);
            if (index + 1 >= tasks.length) {
                return resolve(result);
            }
            ++index;
            run();
        }
        function run() {
            const task = tryfn(tasks[index], index, tasks);
            runPromiseOrReturnRes(task, success, reject);
        }
        run();
    });
}

/**
 * @name 像Array.prototype.reduce一样按序执行异步任务并返回初始值和全部结果
 * @description 只要一个任务报错立即reject，每个任务的结果会按序追加到initialValue，等全部执行完返回全部结果。
 * @description 如果要终止任务，可以截断或者清空tasks，待已启动的最后一个任务执行完后返回全部结果
 */
export function reduce<T>(
    tasks: ((
        previousValue: T[],
        index: number,
        copiedTasks: any[],
    ) => Promise<T[]> | T[])[],
    initialValue: T[] = [],
) {
    return new Promise<T[]>((resolve, reject) => {
        if (!Array.isArray(tasks))
            return reject(new Error('Expected `tasks` to be an array.'));
        if (!Array.isArray(initialValue))
            return reject(new Error('Expected `initialValue` to be an array.'));
        if (!tasks.length) return resolve(initialValue);
        let index = 0;
        tasks = tasks.slice();
        function success(data: T[]) {
            initialValue = data;
            if (index + 1 >= tasks.length) {
                return resolve(initialValue);
            }
            ++index;
            run();
        }
        function run() {
            const task = tryfn(tasks[index], initialValue, index, tasks);
            runPromiseOrReturnRes(task, success, reject);
        }
        run();
    });
}

/**
 * @name 按固定任务数量并发，逐批次完成任务，全部完成后返回全部结果
 */
export function batch<T = any>(
    count: number,
    tasks: ((index: number, batchIndex: number) => T | Promise<T>)[],
) {
    return new Promise<T[]>((resolve, reject) => {
        if (!isFPIN(count)) {
            return reject(
                new Error(
                    'Expected `count` to be a finite positive integer greater than 0.',
                ),
            );
        }
        if (!Array.isArray(tasks))
            return reject(new Error('Expected `tasks` to be an array.'));
        const result: T[] = [];
        if (!tasks.length) return resolve(result);
        const length = Math.ceil(tasks.length / count);
        function tool(index: number) {
            const curTasks = tasks
                .slice(index * count, (index + 1) * count)
                .map((fn, i) => tryfn(fn, index * count + i, index));
            Promise.all(curTasks)
                .then((data) => {
                    result.push(...data);
                    if (index + 1 >= length) {
                        resolve(result);
                    } else {
                        tool(index + 1);
                    }
                })
                .catch(reject);
        }
        tool(0);
    });
}

/**
 * @name 按照指定数量并发第一批任务，每个任务完成后启动一个剩余任务，直至全部完成返回全部结果。
 */
export function outburst<T = any>(
    count: number,
    tasks: Array<(index: number) => T | Promise<T>>,
) {
    return new Promise<T[]>((resolve, reject) => {
        if (!isFPIN(count)) {
            return reject(
                new Error(
                    'Expected `count` to be a finite positive integer greater than 0.',
                ),
            );
        }
        if (!Array.isArray(tasks))
            return reject(new Error('Expected `tasks` to be an array.'));
        const result: T[] = [];
        if (!tasks.length) return resolve(result);
        let index = -1;
        let rejected = false;
        let resolved = 0;
        function failed(error: any) {
            rejected = true;
            reject(error);
        }
        function success(i: number) {
            return (res: T) => {
                if (rejected) return;
                ++resolved;
                result[i] = res;
                if (resolved >= tasks.length) {
                    resolve(result);
                } else if (index + 1 < tasks.length) {
                    run();
                }
            };
        }
        function run() {
            if (rejected) return;
            ++index;
            const task = tryfn(tasks[index], index);
            runPromiseOrReturnRes(task, success(index), failed);
        }
        tasks.slice(0, count).map(run);
    });
}
