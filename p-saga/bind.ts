import { series, map, reduce, batch, outburst } from './index';

Object.defineProperties(
    Promise,
    Object.entries({ series, map, reduce, batch, outburst }).reduce(
        (pre, [name, fn]) => {
            pre[name] = {
                value: fn,
                configurable: false,
                enumerable: false,
                writable: false,
            };
            return pre;
        },
        {} as any,
    ),
);
