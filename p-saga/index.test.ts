import { series, map, reduce, batch, outburst } from './index';

function wait(time: number = 1000, callback?: () => void) {
    return new Promise<number>((resolve) => {
        setTimeout(() => {
            callback?.();
            resolve(time);
        }, time);
    });
}
function waitError(time: number = 1000) {
    return new Promise<number>((_, reject) => {
        setTimeout(() => {
            reject(time);
        }, time);
    });
}
function summation(data: number[], exclude?: number[]) {
    return data.reduce((pre, cur, index) => {
        if (exclude?.includes(index)) {
            return pre;
        }
        return pre + cur;
    }, 0);
}
const timedata = {
    _5s: [300, 500, 1000, 1200, 2000],
    _6s: [300, 500, 1000, 1200, 1000, 2000],
};
const batchErrorMessage = [
    'Expected `count` to be a finite positive integer greater than 0.',
];
const outburstErrorMessage = [
    'Expected `count` to be a finite positive integer greater than 0.',
];

describe('series', () => {
    test('五秒任务', (done) => {
        const times = timedata._5s;
        const startTime = Date.now();
        const totalTime = summation(times);
        let preTime = startTime;
        series(
            times.map((time, index) => (i) => {
                expect(i).toBe(index);
                const now = Date.now();
                if (i !== 0) {
                    expect(now - preTime).toBeGreaterThanOrEqual(times[i - 1]);
                } else {
                    expect(now - preTime).toBeGreaterThanOrEqual(0);
                }
                return wait(time);
            }),
        ).then(() => {
            expect(Date.now() - startTime).toBeGreaterThanOrEqual(totalTime);
            done();
        });
    }, 6000);

    test('同、异步任务混合', (done) => {
        const times = timedata._5s;
        const arr: number[] = [];
        const startTime = Date.now();
        series(
            times.map((n, i) => (j) => {
                arr[j] = n;
                return i % 2 === 0 ? wait(n) : n;
            }),
        ).then((data) => {
            expect(data).toBe(undefined);
            expect(arr.join()).toBe(times.join());
            expect(Date.now() - startTime).toBeGreaterThanOrEqual(
                times.reduce((pre, cur, index) => {
                    if (index % 2 === 0) {
                        pre += cur;
                    }
                    return pre;
                }, 0),
            );
            done();
        });
    });

    test('失败回调', (done) => {
        const times = timedata._5s;
        const sum = summation(times, [4]);
        const startTime = Date.now();
        let preTime = startTime;
        series(
            times.map((time, index) => (i) => {
                expect(i).toBe(index);
                const now = Date.now();
                if (i !== 0) {
                    expect(now - preTime).toBeGreaterThanOrEqual(times[i - 1]);
                } else {
                    expect(now - preTime).toBeGreaterThanOrEqual(0);
                }
                return (index === 3 ? waitError : wait)(time);
            }),
        ).catch((error) => {
            expect(error).toBe(times[3]);
            expect(Date.now() - startTime).toBeGreaterThanOrEqual(sum);
            done();
        });
    }, 6000);

    test('边界：无任务', (done) => {
        const startTime = Date.now();
        series([]).then(() => {
            expect(Date.now() - startTime).toBeLessThan(300);
            done();
        });
    });
});

describe('map', () => {
    test('五秒任务', (done) => {
        const times = timedata._5s;
        const startTime = Date.now();
        let preTime = startTime;
        map(
            times.map((time, index) => (i) => {
                expect(i).toBe(index);
                const now = Date.now();
                if (i !== 0) {
                    expect(now - preTime).toBeGreaterThanOrEqual(times[i - 1]);
                } else {
                    expect(now - preTime).toBeGreaterThanOrEqual(0);
                }
                return index === 4 ? time : wait(time);
            }),
        ).then((results) => {
            expect(summation(results)).toBe(summation(times));
            expect(Date.now() - startTime).toBeGreaterThanOrEqual(
                summation(times, [4]),
            );
            done();
        });
    }, 6000);

    test('同、异步任务混合', (done) => {
        const times = timedata._5s;
        const arr: number[] = [];
        const startTime = Date.now();
        map(
            times.map((n, i) => (j) => {
                arr[j] = n;
                return i % 2 === 0 ? wait(n) : n;
            }),
        ).then((results) => {
            expect(summation(results)).toBe(summation(times));
            expect(arr.join()).toBe(times.join());
            expect(Date.now() - startTime).toBeGreaterThanOrEqual(
                times.reduce((pre, cur, index) => {
                    if (index % 2 === 0) {
                        pre += cur;
                    }
                    return pre;
                }, 0),
            );
            done();
        });
    });

    test('失败回调', (done) => {
        const times = timedata._5s;
        const startTime = Date.now();
        let preTime = startTime;
        map(
            times.map((time, index) => (i) => {
                expect(i).toBe(index);
                const now = Date.now();
                if (i !== 0) {
                    expect(now - preTime).toBeGreaterThanOrEqual(times[i - 1]);
                } else {
                    expect(now - preTime).toBeGreaterThanOrEqual(0);
                }
                return (index === 3 ? waitError : wait)(time);
            }),
        ).catch((error) => {
            expect(error).toBe(times[3]);
            expect(Date.now() - startTime).toBeGreaterThanOrEqual(
                summation(times.slice(0, -1)),
            );
            done();
        });
    }, 6000);

    test('边界：无任务', (done) => {
        const startTime = Date.now();
        map([]).then(() => {
            expect(Date.now() - startTime).toBeLessThan(300);
            done();
        });
    });
});

describe('reduce', () => {
    test('五秒任务', (done) => {
        const times = timedata._6s;
        const startTime = Date.now();
        let preTime = startTime;
        reduce<number>(
            times.map((time, index) => (pre, i, tasks) => {
                expect(i).toBe(index);
                expect(pre.length).toBe(i);
                const now = Date.now();
                if (i < 4 && i > 0) {
                    expect(now - preTime).toBeGreaterThanOrEqual(
                        summation(times.slice(1, i)),
                    );
                }
                pre.push(time);
                return wait(time).then(() => pre);
            }),
            [],
        ).then((results) => {
            expect(summation(results)).toBe(summation(times));
            expect(Date.now() - startTime).toBeGreaterThanOrEqual(
                summation(times),
            );
            done();
        });
    }, 7000);

    test('失败回调', (done) => {
        const times = timedata._5s;
        reduce<number>(
            times.map((time, index) => (pre, i, tasks) => {
                expect(i).toBe(index);
                return (index === 3 ? waitError : wait)(time).then(() => pre);
            }),
            [],
        ).catch((error) => {
            expect(error).toBe(times[3]);
            done();
        });
    }, 6000);

    test('边界：无任务', (done) => {
        const startTime = Date.now();
        reduce([]).then(() => {
            expect(Date.now() - startTime).toBeLessThan(300);
            done();
        });
    });
});

describe('batch', () => {
    test('五秒任务', (done) => {
        const times = [300, 500, 1000, 1200, 2000, 1300, 1300, 1000];
        const sum = summation(times);
        const allTime = 5000;
        const startTime = Date.now();
        batch(
            2,
            times.map((time, index) => (i, d) => {
                expect(i).toBe(index);
                expect(d).toBe(i % 2 ? (i + 1) / 2 - 1 : i / 2);
                return index === 5 ? time : wait(time);
            }),
        ).then((results) => {
            expect(summation(results)).toBe(sum);
            expect(Date.now() - startTime).toBeGreaterThanOrEqual(allTime);
            done();
        });
    }, 10000);

    test('失败回调', (done) => {
        const times = [300, 500, 1000, 1200, 2000];
        const startTime = Date.now();
        batch<number>(
            2,
            times.map((time, index) => (i, d) => {
                expect(i).toBe(index);
                expect(d).toBe(i % 2 ? (i + 1) / 2 - 1 : i / 2);
                return (index === 3 ? waitError : wait)(time);
            }),
        ).catch((error) => {
            expect(error).toBe(times[3]);
            expect(Date.now() - startTime).toBeGreaterThanOrEqual(
                times[3] + times[1],
            );
            done();
        });
    }, 3000);

    test('边界：只有一批次', (done) => {
        const times = [300, 500, 1000, 1200, 2000];
        const sum = times.reduce((pre, cur) => pre + cur, 0);
        const startTime = Date.now();
        batch<number>(
            times.length,
            times.map((time, index) => (i, d) => {
                expect(i).toBe(index);
                expect(d).toBe(0);
                return wait(time);
            }),
        ).then((results) => {
            expect(summation(results)).toBe(sum);
            expect(Date.now() - startTime).toBeGreaterThanOrEqual(
                Math.max(...times),
            );
            done();
        });
    }, 6000);

    test('边界：无任务', (done) => {
        const startTime = Date.now();
        batch(2, []).then(() => {
            expect(Date.now() - startTime).toBeLessThan(300);
            done();
        });
    }, 1000);

    test('边界：第一个参数是小数', (done) => {
        batch(2.2, []).catch((error) => {
            expect(error?.message).toBe(batchErrorMessage[0]);
            done();
        });
    }, 1000);

    test('边界：第一个参数是Infinity', (done) => {
        batch(Infinity, []).catch((error) => {
            expect(error?.message).toBe(batchErrorMessage[0]);
            done();
        });
    }, 1000);

    test('边界：第一个参数是负数', (done) => {
        batch(-2, []).catch((error) => {
            expect(error?.message).toBe(batchErrorMessage[0]);
            done();
        });
    }, 1000);

    test('边界：第二个参数中有异常成员', (done) => {
        const times = [3000, 500, 2000, 1200, 1000, 1300];
        batch(
            2,
            times.map((time, index) =>
                index === 4 ? ({} as any) : () => wait(time),
            ),
        ).catch((error) => {
            expect(error instanceof Error).toBe(true);
            done();
        });
    }, 6000);
});

describe('outburst', () => {
    test('五秒任务', (done) => {
        const times = [3000, 500, 2000, 1200, 1000, 1300, 1000];
        const sum = summation(times);
        const resolveIndex = [1, 2, 0, 3, 4, 6, 5];
        const startTime = Date.now();
        const indexs: number[] = [];
        outburst(
            2,
            times.map((time, index) => (i) => {
                if (index === 6) {
                    indexs.push(i);
                    return time;
                }
                return wait(time, () => {
                    indexs.push(i);
                });
            }),
        ).then((results) => {
            expect(summation(results)).toBe(sum);
            expect(results.join(',')).toBe(times.join(','));
            expect(indexs.join(',')).toBe(resolveIndex.join(','));
            const endTime = Date.now();
            expect(endTime - startTime).toBeGreaterThanOrEqual(5000);
            expect(endTime - startTime).toBeLessThanOrEqual(6000);
            done();
        });
    }, 6000);

    test('失败回调', (done) => {
        const times = [3000, 500, 2000, 1200, 1000, 1300];
        const startTime = Date.now();
        outburst(
            2,
            times.map(
                (time, index) => () => (index === 4 ? waitError : wait)(time),
            ),
        ).catch((error) => {
            expect(error).toBe(times[4]);
            const endTime = Date.now();
            expect(endTime - startTime).toBeGreaterThanOrEqual(4000);
            expect(endTime - startTime).toBeLessThanOrEqual(5000);
            done();
        });
    }, 5000);

    test('边界：无任务', (done) => {
        const startTime = Date.now();
        outburst(2, []).then((results) => {
            expect(results.length).toBe(0);
            const endTime = Date.now();
            expect(endTime - startTime).toBeGreaterThanOrEqual(0);
            expect(endTime - startTime).toBeLessThanOrEqual(300);
            done();
        });
    }, 1000);

    test('边界：一个任务', (done) => {
        const startTime = Date.now();
        outburst(2, [() => wait()]).then((results) => {
            expect(results[0]).toBe(1000);
            const endTime = Date.now();
            expect(endTime - startTime).toBeGreaterThanOrEqual(1000);
            expect(endTime - startTime).toBeLessThanOrEqual(1300);
            done();
        });
    }, 1300);

    test('边界：第一个参数是小数', (done) => {
        outburst(2.2, []).catch((error) => {
            expect(error?.message).toBe(outburstErrorMessage[0]);
            done();
        });
    }, 1000);

    test('边界：第一个参数是Infinity', (done) => {
        outburst(Infinity, []).catch((error) => {
            expect(error?.message).toBe(outburstErrorMessage[0]);
            done();
        });
    }, 1000);

    test('边界：第一个参数是负数', (done) => {
        outburst(-2, []).catch((error) => {
            expect(error?.message).toBe(outburstErrorMessage[0]);
            done();
        });
    }, 1000);

    test('边界：第二个参数中有异常成员', (done) => {
        const times = [3000, 500, 2000, 1200, 1000, 1300];
        outburst(
            2,
            times.map((time, index) =>
                index === 4 ? ({} as any) : () => wait(time),
            ),
        ).catch((error) => {
            expect(error instanceof Error).toBe(true);
            done();
        });
    }, 6000);
});
