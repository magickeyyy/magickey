const path = require("path");
const fs = require("fs/promises");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { WebpackManifestPlugin } = require("webpack-manifest-plugin");

/**
 * @name 服务器部署路径
 */
const PublicPath = "/";
const IsProd = process.env.NODE_ENV === "development";

console.log("process.env.NODE_ENV:", process.env.NODE_ENV);

module.exports = {
    mode: process.env.NODE_ENV,
    entry: "./src/index.ts",
    devtool: IsProd ? undefined : "inline-source-map",
    stats: "errors-only",
    output: {
        publicPath: PublicPath,
        filename: "bundle.js",
        path: path.resolve(__dirname, "dist"),
        clean: true,
    },
    resolve: {
        alias: {
            "@": path.resolve(__dirname, "src"),
            "#": __dirname,
        },
        // 会按这个顺序解析文件
        extensions: [".ts", "..."],
    },
    module: {
        rules: [
            { test: /\.css$/, use: ["style-loader", "css-loader"] },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: "asset/resource",
            },
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/,
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "template.ejs"),
            inject: "body",
            meta: {
                viewport:
                    "width=device-width, initial-scale=1, shrink-to-fit=no",
                "theme-color": "#4285f4",
            },
            title: "fetch",
            favicon: "favicon.ico",
        }),
        new WebpackManifestPlugin({}),
    ],
    devServer: {
        static: "./dist",
        port: 3000,
        compress: true,
        client: {
            overlay: {
                errors: true,
                warnings: false,
            },
            progress: true,
            reconnect: true,
        },
    },
};
