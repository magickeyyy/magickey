import axios from "axios";
/**
 * @name HTTP请求方法名
 */
export type RequestMethod =
    | "GET"
    | "POST"
    | "PUT"
    | "DELETE"
    | "PATCH"
    | "HEAD"
    | "OPTIONS"
    | "PURGE"
    | "TRACE";

export type ResponseType =
    | "arraybuffer"
    | "blob"
    | "document"
    | "json"
    | "text"
    | "stream";

export type ResponseEncoding =
    | "ascii"
    | "ASCII"
    | "ansi"
    | "ANSI"
    | "binary"
    | "BINARY"
    | "base64"
    | "BASE64"
    | "base64url"
    | "BASE64URL"
    | "hex"
    | "HEX"
    | "latin1"
    | "LATIN1"
    | "ucs-2"
    | "UCS-2"
    | "ucs2"
    | "UCS2"
    | "utf-8"
    | "UTF-8"
    | "utf8"
    | "UTF8"
    | "utf16le"
    | "UTF16LE";
export type RequestInitConfig = { method?: RequestMethod } & Omit<
    RequestInit,
    "method"
>;
export interface AxiosBasicCredentials {
    username: string;
    password: string;
}
export interface AxiosRequestTransformer {
    (this: FetchInstance, data: any, headers: any): any;
}

export interface AxiosResponseTransformer {
    (this: FetchInstance, data: any, headers: any, status?: number): any;
}
export type FetchHeaders = Record<string, string> | [string, string][];
export type FetchCreatConfig<T = any> = {
    url?: string;
    method?: RequestMethod;
    baseURL?: string;
    transformRequest?: AxiosRequestTransformer | AxiosRequestTransformer[];
    transformResponse?: AxiosResponseTransformer | AxiosResponseTransformer[];
    headers?: FetchHeaders;
    params?: any;
    data?: T;
    timeout?: number;
    timeoutErrorMessage?: string;
    withCredentials?: boolean;
    auth?: AxiosBasicCredentials;
    responseType?: ResponseType;
    responseEncoding?: ResponseEncoding | string;
    xsrfCookieName?: string;
    xsrfHeaderName?: string;
    maxContentLength?: number;
    validateStatus?: ((status: number) => boolean) | null;
    maxBodyLength?: number;
    maxRedirects?: number;
    beforeRedirect?: (
        options: Record<string, any>,
        responseDetails: { headers: Record<string, string> }
    ) => void;
    decompress?: boolean;
};
axios.create;
function request() {
    return fetch("");
}
function create(config?: FetchCreatConfig) {
    return new FetchInstance(config);
}
function createRequest(input: RequestInfo | URL, init?: RequestInitConfig) {
    const request = new Request(input, init);
    return request;
}

/**
 * @name 生成header
 */
function creatHeaders(config: FetchHeaders) {
    const headers = new Headers();
    if (Array.isArray(config)) {
        config.map(([key, value]) => {
            headers.append(key, value);
        });
    } else {
        Object.entries(config).map(([key, value]) => {
            headers.append(key, value);
        });
    }
    return headers;
}

function serializeParams() {}

class FetchInstance {
    #baseURL?: string;
    get baseURL() {
        return this.#baseURL;
    }
    #url?: string;
    get url() {
        return this.#url;
    }
    #method: RequestMethod;
    get method() {
        return this.#method;
    }
    /**
     * @name 默认请求头
     * @description 具体请求的headers会和默认请求头合并
     */
    #headers: FetchHeaders;
    get headers() {
        return this.#headers;
    }
    /**
     * @name 超时时长
     * @description 超过设定值就失败。默认0，永不超时
     */
    #timeout: number;
    get timeout() {
        return this.#timeout;
    }
    constructor({
        baseURL,
        url,
        method = "GET",
        headers = {},
        timeout = 0,
    }: FetchCreatConfig = {}) {
        this.#baseURL = baseURL;
        this.#url = url;
        this.#method = method;
        this.#headers = headers;
        this.#timeout = timeout;
    }
}
const headers = creatHeaders([
    ["1", "11"],
    ["1", "22"],
    ["12", "3"],
]);
console.log(":", headers.get("1"));

fetch(
    createRequest("/", {
        headers: creatHeaders([
            ["1", "1"],
            ["1", "2"],
            ["12", "3"],
        ]),
    })
);
const headerss = creatHeaders({
    "content-type": "application/x-www-form-urlencoded",
});
const data = new URLSearchParams();

export { creatHeaders, createRequest };
