import { isString } from 'lodash';

function isRecord<T extends Record<string, any> = Record<string, any>>(
    data?: T,
): data is T {
    return Object.prototype.toString.call(data) === '[object Object]';
}

function isVersion(data?: any): data is Version {
    return (
        isString(data) &&
        data.split('.').every((str) => /^(0|[1-9]+\d*)$/.test(str))
    );
}
function isPositiveNumber(data?: any): data is number {
    return typeof data === 'number' && !Number.isNaN(data) && data >= 0;
}
function isExpires<T>(data?: any): data is Expires<T> {
    return (
        isPositiveNumber(data) ||
        (isRecord(data) &&
            Object.values(data).every((v) => isPositiveNumber(v)))
    );
}

export { isRecord, isVersion, isPositiveNumber, isExpires };
