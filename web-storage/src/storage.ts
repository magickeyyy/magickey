/**
 * @name:
 * @description 字段有前缀，开发环境比较有用，或者接入了第三方库会操作storage避免冲突，或者需要多个独立的storage
 * @description 字段有过期时间
 * @description 有版本号
 * @description localStorage有跨页面通信能力
 */

import { isFunction, isString } from 'lodash';
import { isExpires, isRecord, isVersion } from './utils';

class MagicStorage<
    T extends Partial<CanStringifyRecord> = Partial<CanStringifyRecord>,
> {
    constructor(config: StorageConfig<T>) {
        const {
            type = 'localStorage',
            version,
            namespace,
            storageEvent = false,
            store,
            preState,
            expires,
        } = config;
        this.set = this.set.bind(this);
        this.#type = type;
        this.storageEvent = storageEvent;
        if (
            isString(namespace) &&
            namespace.trim() &&
            !namespace.includes(this.#separator)
        ) {
            this.#namespace = namespace;
        }
        const _state = preState?.(this.#getPreSate());
        this.version = _state?.version ?? version;
        this.expires = _state?.expires ?? expires;
        const _store = _state?.store ?? store;
        if (isRecord(_store)) {
            this.set(_store);
        }
    }
    /**
     * @name storage类型
     * @default localStorage
     */
    #type: 'localStorage' | 'sessionStorage';
    get type() {
        return this.#type;
    }
    #namespace: string = 'magic';
    /**
     * @name 字段key前缀
     * @default {string} magic
     * @description 一般是项目名，主要是避免命名冲突，比如常用的开发环境
     * @description 初始化时如果配置了前缀，只会提取满足前缀的字段
     * @description 更新前缀，只会提取满足前缀的字段，更新store
     */
    get namespace() {
        return this.#namespace;
    }
    #expires?: Expires<T>;
    #expiresKey: '_expires' = '_expires';
    get expiresKey() {
        return this.prefix + this.#expiresKey;
    }
    /**
     * @name 过期时间
     * @description 要么给个store加过期时间，要么给部分字段加过期时间
     */
    get expires() {
        return this.#expires;
    }
    set expires(data: Expires<T> | undefined) {
        const key = this.#transformKey(this.#expiresKey);
        this.storage.removeItem(key);
        if (!data) {
            this.#expires = undefined;
        } else if (isExpires(data)) {
            this.#expires = data;
            this.storage.setItem(key, JSON.stringify(data));
        }
    }
    #version?: Version;
    #versionKey = '_version';
    get versionKey() {
        return this.prefix + this.#versionKey;
    }
    /**
     * @name 版本号
     * @type {`${number}.${number}.${number}`} 必须是三段，每段要么是0要么是1-9开头的数字
     * @description 会触发version钩子
     * @description 初始化时配置了版本号，只会提取满足版本号的字段
     * @description 更新版本号，只会提取满足版本号的字段，更新store
     */
    get version() {
        return this.#version;
    }
    set version(data: Version | undefined) {
        const key = this.#transformKey(this.#versionKey);
        if (!data) {
            this.#version = undefined;
            this.storage.removeItem(key);
        } else if (isVersion(data)) {
            this.#version = data;
            this.storage.setItem(key, JSON.stringify(data));
        }
    }
    get storage() {
        return window[this.#type];
    }
    /**
     * @name 是否监听storage事件
     * @description 如果是localStorage，开启多个页面时，可能需要监听其他页面的数据变化。当某个页面的localStorage更新了，其他页面也会更新。
     */
    #storageEvent: boolean = false;
    get storageEvent() {
        return this.#storageEvent;
    }
    set storageEvent(data: boolean) {
        if (this.#storageEvent === data) return;
        this.#storageEvent = data;
        data ? this.#handleStorageEvent() : this.#unsubscribeStorageEvent?.();
    }
    #unsubscribeStorageEvent?: () => void;
    /**
     * @name key中的间隔符
     */
    #separator = '|';
    get prefix() {
        return this.#namespace + this.#separator;
    }
    #store: T = {} as T;
    get store() {
        return this.#store;
    }
    /**
     * @name 根据参数补充前缀或者移除前缀
     */
    #transformKey(key: string) {
        return key.startsWith(this.prefix)
            ? key.replace(this.prefix, '')
            : this.prefix + key;
    }
    #handleStorageEvent() {
        this.#unsubscribeStorageEvent?.();
        const handler = (event: StorageEvent) => {
            console.log('event:', event);
            const { key, newValue, oldValue } = event;
            if (!this.#isMyKey(key)) return;
            if (Object.is(newValue, oldValue)) return;
            if (key === this.versionKey) {
                if (newValue === null) return;
                this.#version = JSON.parse(newValue);
                return;
            }
            if (key === this.expiresKey) {
                if (newValue === null) return;
                this.#expires = JSON.parse(newValue);
                return;
            }
            if (this.#storageTimer !== null) {
                clearTimeout(this.#storageTimer);
                this.#storageTimer = null;
            }
            this.#storageEventData[key] = newValue;
            this.#storageTimer = setTimeout(this.#effectOfStorageEvent, 250);
        };
        window.addEventListener('storage', handler);
        this.#unsubscribeStorageEvent = () =>
            window.removeEventListener('storage', handler);
    }
    #effectOfStorageEvent() {
        let updated = false;
        Object.entries(this.#storageEventData).forEach(([key, value]) => {
            let _value: any;
            try {
                _value === JSON.parse(value);
            } catch (e) {}
            const _key = key.replace(this.prefix, '') as keyof T;
            updated = true;
            if (_value === undefined) {
                delete this.#store[_key];
            } else {
                this.#store[_key] = _value;
            }
        });
        updated && this.#runChangeCallbacks();
    }
    /**
     * @name:
     * @description 校验一个字符串是不是store、expires、version的key
     */
    #isMyKey(key?: any): key is string {
        if (!isString(key) || !key.startsWith(this.prefix)) return false;
        const rest = key.replace(this.prefix, '');
        if (!rest.trim() || rest.includes(this.#separator)) return false;
        return true;
    }
    #getPreSate() {
        return Object.entries(this.storage).reduce((pre, [key, string]) => {
            if (
                !(
                    key.startsWith(this.prefix) &&
                    key.length > this.prefix.length
                )
            )
                return pre;
            const _key = key.replace(this.prefix, '');
            if (!_key.trim() || _key.includes(this.#separator)) return pre;
            if (_key === this.#versionKey) {
                pre.version = JSON.parse(string);
            } else if (_key === this.#expiresKey) {
                pre.expires = JSON.parse(string);
            } else {
                if (!pre.store) {
                    pre.store = {};
                }
                pre.store[_key as keyof T] = JSON.parse(string);
            }
            return pre;
        }, {} as PreState<T>);
    }
    #emitChangeTimer: NodeJS.Timeout | null = null;
    #storageTimer: NodeJS.Timeout | null = null;
    #storageEventData: Record<string, any> = {};
    #runChangeCallbacks() {
        if (typeof this.#emitChangeTimer === 'number') {
            clearTimeout(this.#emitChangeTimer);
        }
        this.#emitChangeTimer = setTimeout(() => {
            this.#changeCallbacks.forEach((callback) => callback(this.store));
        });
    }
    #changeCallbacks: StoreChangeEvent<T>[] = [];
    /**
     * @name 监听store变化
     * @param {StoreChangeEvent} callback
     */
    on(callback: StoreChangeEvent<T>) {
        if (!this.#changeCallbacks.find((v) => v === callback)) {
            this.#changeCallbacks.push(callback);
        }
    }
    /**
     * @name 取消store监听
     */
    off(callback: StoreChangeEvent<T>) {
        const index = this.#changeCallbacks.findIndex((v) => v === callback);
        if (index > -1) {
            this.#changeCallbacks.splice(index, 1);
        }
    }
    /**
     * @name 解除store全部监听
     */
    offAll() {
        this.#changeCallbacks.length = 0;
    }
    /**
     * @name 修改store和storage
     * @description 传对象 合并旧store和value
     * @description 传个函数，会以返回值覆盖store
     * @description 参数只有一个string，会移除这个键
     * @description 第一个参数是string，有第二个参数，更新store一个键
     */
    set(value: T): void;
    set(reducer: (curStore: T) => T): void;
    set<K extends keyof T>(key: K, value?: T[K]): void;
    set(key: any, value?: any) {
        if (isString(key) && !key.includes(this.#separator)) {
            if (Object.is(this.#store[key], value)) return;
            if (value === undefined) {
                this.storage.removeItem(this.#transformKey(key));
                delete this.store[key];
                return;
            }
            this.storage.setItem(
                this.#transformKey(key),
                JSON.stringify(value),
            );
            this.#store[key as keyof T] = value;
        } else if (isFunction(key)) {
            const store = (key as any)(this.#store);
            this.#store = {} as any;
            Object.entries<any>(store).forEach(([key, value]) => {
                this.set(key, value);
            });
        } else if (isRecord(key)) {
            Object.entries<any>(key).forEach(([key, value]) => {
                this.set(key, value);
            });
        }
        this.#runChangeCallbacks();
    }
}

export { MagicStorage };
