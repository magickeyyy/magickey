import { MagicStorage } from './storage';
import './styles/page.scss';
const p = document.createElement('p');
const text = document.createTextNode('just do it');
p.appendChild(text);
document.body.appendChild(p);

const store = { a: 1, b: { aa: [1, 2, 3] } };
const storage = new MagicStorage<{
    a: number;
    b: { aa: number[] };
    c?: string[];
}>({
    type: 'localStorage',
    namespace: 'admin',
    version: '1.0.1',
    store,
    expires: { a: Date.now() },
    preState: (p) => {
        console.log('p:', p);
        return p;
    },
});
const cb = (store: any) => {
    console.log('store:', store);
};
storage.storageEvent = true;
storage.on(cb);

p.addEventListener('click', () => {
    storage.set('a', Math.random());
});
