import { MagicStorage } from './storage';
import './styles/global.scss';
const p = document.createElement('p');
const text = document.createTextNode('just do it');
p.appendChild(text);
document.body.appendChild(p);

const store = { a: 1, b: { aa: [1, 2, 3] } };
const storage = new MagicStorage<{
    a: number;
    b: { aa: number[] };
    c?: string[];
}>({
    type: 'localStorage',
    namespace: 'admin',
    version: '1.0.1',
    store,
    expires: { a: Date.now() },
    preState: (p) => {
        console.log('p:', p);
        return p;
    },
});
const cb = (store: any) => {
    console.log('store:', store);
};
storage.storageEvent = true;
storage.on(cb);

p.addEventListener('click', () => {
    storage.set('a', Math.random());
});

function* generate() {
    console.log('*1:');
    yield new Promise<void>((resolve) => {
        resolve();
    }).then(() => {
        console.log('*1111:');
    });
    console.log('*2:');
    yield new Promise<void>((resolve) => {
        resolve();
    }).then(() => {
        console.log('*2222:');
    });
    console.log('*3:');
    return new Promise<void>((resolve) => {
        resolve();
    }).then(() => {
        console.log('*3333:');
    });
}
function promise() {
    return new Promise<void>((resolve) => {
        resolve();
    }).then(() => {
        console.log('p0000:');
    });
}
async function asyncf() {
    console.log('a1:');
    await new Promise<void>((resolve) => {
        resolve();
    }).then(() => {
        console.log('a1111:');
    });
    console.log('a2:');
    await new Promise<void>((resolve) => {
        resolve();
    }).then(() => {
        console.log('a2222:');
    });
    console.log('a3:');
    return new Promise<void>((resolve) => {
        resolve();
    }).then(() => {
        console.log('a3333:');
    });
}

const tick = generate();
promise();
asyncf();
for (let next of tick) {
}
