function isStrictObject(data?: any) {
    return Object.prototype.toString.call(data) === '[object Object]';
}

class Listener {
    #timer: NodeJS.Timeout | null = null;
    #callbacks: Array<() => void> = [];
    on(callback: () => void) {
        if (this.#callbacks.includes(callback)) return;
        this.#callbacks.push(callback);
    }
    off(callback: () => void) {
        const index = this.#callbacks.findIndex(callback);
        if (index !== -1) {
            this.#callbacks.splice(index, 1);
        }
    }
    /**
     * @description 用timer避免批量操作时多次触发
     */
    call() {
        if (this.#timer !== null) {
            clearTimeout(this.#timer);
            this.#timer = null;
        }
        this.#timer = setTimeout(() => {
            this.#callbacks.forEach((fn) => fn());
        }, 0);
    }
}

/**
 * @name 已代理对象监听回调查重
 * @description 每个被监听的已代理对象的回调都在这里
 */
const ProxyListenerMap = new WeakMap<object, Listener>();
/**
 * @name 已代理对象查重
 * @description 每个被代理过得对象都往这里放一下，查重
 */
const ProxyTargetMap = new WeakMap();
/**
 * @description 没法剔除NaN，但是需要剔除NaN
 */
type CanStringifyBase = undefined | null | boolean | number | string;
type CanStringifyArray<T = CanStringifyBase> =
    | (T | CanStringifyArray<T> | CanStringifyRecord<T>)[];

/**
 * @name store类型
 * @description 保证store至少是一个{}对象，键值是被序列化后类型不失真（序列化后反序列化，源数据不变。undefined会移除键，NaN会变成null，这些影响不考虑）
 */
interface CanStringifyRecord<T = CanStringifyBase> {
    [K: string]: T | CanStringifyRecord<T> | CanStringifyArray<T>;
}
function isObject(x: unknown): x is object {
    return typeof x === 'object' && x !== null;
}

// function canProxy(x: unknown) {
//     return (
//         isObject(x) &&
//         !ProxyTargetSet.has(x) &&
//         (Array.isArray(x) || !(Symbol.iterator in x)) &&
//         !(x instanceof WeakMap) &&
//         !(x instanceof WeakSet) &&
//         !(x instanceof Error) &&
//         !(x instanceof Number) &&
//         !(x instanceof Date) &&
//         !(x instanceof String) &&
//         !(x instanceof RegExp) &&
//         !(x instanceof ArrayBuffer)
//     );
// }

const canStringifyBase = (x: unknown): x is CanStringifyBase =>
    x === undefined ||
    x === null ||
    (typeof x === 'number' && !Number.isNaN(x)) ||
    typeof x === 'string';

const isProxyObject = (x: unknown): x is object =>
    Array.isArray(x) || isStrictObject(x);
const canProxy = (x: unknown): x is object =>
    Array.isArray(x) || Object.prototype.toString.call(x) === '[object Object]';

/**
 * @name:
 * @description 原生不支持代理Map、Set，使用代理对象的成员this指向不一定正确。所以成员不能是Map、Set、function、Promise
 */
function creatProxy<T extends object>(data: T = {} as T): T {
    if (!canProxy(data)) {
        throw new Error(`object required`);
    }
    return proxyDeep(data);
}

function createHandler<T extends object>(): ProxyHandler<T> {
    return {
        set(target, key, value, receiver) {
            let curValue = value;
            if (Array.isArray(value) || isStrictObject(value)) {
                curValue = ProxyTargetMap.get(value) || proxyDeep(value);
                if (ProxyTargetMap.get(value)) {
                    curValue = ProxyTargetMap.get(value);
                } else {
                    const proxy = proxyDeep(value);
                    ProxyTargetMap.set(value, proxy);
                    curValue = proxy;
                }
            }
            const preValue = Reflect.get(target, key, receiver);
            if (
                !Object.is(curValue, preValue) &&
                ProxyListenerMap.has(receiver)
            ) {
                ProxyListenerMap.get(receiver)?.call();
            }
            return Reflect.set(target, key, curValue, receiver);
        },
        deleteProperty(target, key) {
            ProxyListenerMap.get(target)?.call();
            return Reflect.deleteProperty(target, key);
        },
    };
}
/**
 * @name
 * @description JSON序列化后类型会变样，或者不能保证源数据。
 * @description 基本类型只有null、undefined、number、string，复杂类型只有{}和[]
 * @description 也不会用proxy代理这些类型的数据
 */
function cannotStringiy(data: unknown) {
    return (
        typeof data === 'bigint' ||
        typeof data === 'function' ||
        typeof data === 'symbol' ||
        Number.isNaN(data) ||
        data instanceof Promise ||
        data instanceof Map ||
        data instanceof Set ||
        data instanceof WeakMap ||
        data instanceof WeakSet
    );
}

function proxyDeep(data: any): any {
    if (ProxyTargetMap.has(data)) {
        return ProxyTargetMap.get(data);
    }
    if (Array.isArray(data)) {
        const proxy = new Proxy(
            data.map((item) => proxyDeep(item)),
            createHandler(),
        );
        ProxyTargetMap.set(data, proxy);
        return proxy;
    } else if (isStrictObject(data)) {
        const proxy = new Proxy(
            Object.keys(data).reduce((pre, key) => {
                pre[key] = proxyDeep(data[key]);
                return pre;
            }, {} as any),
            createHandler(),
        );
        ProxyTargetMap.set(data, proxy);
        return proxy;
    } else if (canStringifyBase(data)) {
        return data;
    } else {
        throw new Error('stringify data cannot renew');
    }
}

/**
 * @name 订阅一个proxy
 * @description 只要proxy是对象或数组，更新了就会触发回调。
 * @description 回调必定是在后面的EventLoop中响应，不适用于需要快速响应的场景
 * @param {object} proxy 必须是对象或数组
 * @param {()=>void} callback proxy的子属性值发生变化时的回调，批量更新
 * @return {()=>void} unSubscribe 手动取消订阅
 */
function subscribe<T extends object>(proxy: T, callback: () => void) {
    if (ProxyListenerMap.has(proxy)) {
        ProxyListenerMap.get(proxy)?.on(callback);
    } else {
        const listener = new Listener();
        listener.on(callback);
        ProxyListenerMap.set(proxy, listener);
    }
    return function () {
        if (ProxyListenerMap.has(proxy)) {
            ProxyListenerMap.get(proxy)?.off(callback);
        }
    };
}

/**
 * @name 取消订阅
 */
function unSubscribe<T extends object>(proxy: T, callback: () => void) {
    if (ProxyListenerMap.has(proxy)) {
        ProxyListenerMap.get(proxy)?.off(callback);
    }
}

export { creatProxy, subscribe, unSubscribe };
