const IsDev = process.argv[2] === 'serve';
const TITLE = IsDev ? 'development' : 'production';
const OUTPUT_DIR = 'dist';
const PUBLIC_DIR = 'public';
const PUBLIC_PATH = '/';
const FilenameTemplate = '[name].[contenthash]';

module.exports = {
    IsDev,
    TITLE,
    OUTPUT_DIR,
    PUBLIC_DIR,
    PUBLIC_PATH,
    FilenameTemplate,
};
