const fs = require('fs');
const path = require('path');
const { fileTypeFromStream } = require('file-type');

const png = fs.createReadStream(
    path.join(process.cwd(), 'src/images/comment2.png'),
);
fileTypeFromStream(png).then((value) => {
    console.log('value:', value);
});
