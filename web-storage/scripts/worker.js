const p_saga = require('p-saga');

const primen = [];
function isPrimeNumber(x) {
    let is = true;
    if (primen.includes(x)) return is;
    for (let i = 1; i <= x; i++) {
        if (x % i === 0 && i !== 1 && i !== x) {
            is = false;
            break;
        }
    }
    if (is && !primen.includes(x)) {
        primen.push(x);
    }
    return is;
}
function primeNumber(start, end) {
    const res = [];
    for (let i = start; i <= end; i++) {
        if (isPrimeNumber(i)) {
            res.push(i);
        }
    }
    return res;
}
function splitTasks(start, end) {
    const max = 1000;
    const total = end - start;
    const argvs = [];
    const count = Math.ceil(total / max);
    for (let i = 0; i < count; i++) {
        const st = i * max + 1;
        const en = (i + 1) * max;
        if (en > end) {
            argvs.push([st, end]);
        } else {
            argvs.push([st, en]);
        }
    }
    return argvs;
}
// const primes = primeNumber(1, 1000);
// console.log(':', splitTasks(1, 10_010));
const tasks = splitTasks(1, 10_000_000).map(
    (argvs) => () =>
        new Promise((resolve) => {
            console.log('argvs:', argvs);
            resolve(primeNumber(...argvs));
        }),
);
p_saga.map(tasks).then((res) => {
    console.log(':', res.flat(Infinity));
});
