require('colors');
const osu = require('node-os-utils');

const cpu = osu.cpu;
const memory = osu.mem;

function statsInfo() {
    return Promise.all([
        memory.free(),
        memory.used(),
        cpu.count(),
        cpu.free(),
        cpu.usage(),
    ]).then(
        ([
            { totalMemMb, freeMemMb },
            { usedMemMb },
            count,
            cpuFree,
            cpuUsed,
        ]) => {
            console.table([
                {
                    CPU线程总数: count,
                    CPU闲置率: cpuFree.toFixed(2) + '%',
                    CPU使用率: cpuUsed.toFixed(2) + '%',
                    内存总数: (totalMemMb / 1024).toFixed(2) + 'G',
                    已使用内存: (usedMemMb / 1024).toFixed(2) + 'G',
                    未已使用内存: (freeMemMb / 1024).toFixed(2) + 'G',
                },
            ]);
        },
    );
}
statsInfo();
