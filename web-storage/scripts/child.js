const { spawn } = require('child_process');
process.on('beforeExit', (code) => {
    console.log('beforeExit:', code);
});
process.on('exit', (code) => {
    console.log('exit:', code);
});
process.on('uncaughtException', (error, origin) => {
    console.log('uncaughtException:', error, origin);
});
process.on('unhandledRejection', (reason, promise) => {
    console.log('unhandledRejection:', reason, promise);
});
process.on('rejectionHandled', (promise) => {
    console.log('rejectionHandled:', promise);
});
process.on('warning', (warning) => {
    console.log('warning:', warning);
});
process.on('worker', (worker) => {});
process.on('message', (message, sendHandle) => {});

const child_process = spawn('node', ['./scripts/child']);
child_process.stdout.on('data', (chunk) => {
    console.log('子进程日志:', chunk);
});
child_process.stderr.on('data', (chunk) => {
    console.log('子进程错误日志:', chunk);
});
