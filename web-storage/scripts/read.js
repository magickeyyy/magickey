const path = require('path');
const os = require('os');
const fs = require('fs');
const fs_p = require('fs/promises');
const fs_e = require('fs-extra');
const symlinkOrCopySync = require('symlink-or-copy').sync;
const figlet = require('figlet');

const readdirOptions = { withFileTypes: true };
const IsWindows = process.platform === 'win32';

/**
 * @name 遍历目录
 * @description 获取目录下文件夹、文件路径，忽略软链接
 * @param {string} dir 目录路径
 * @param {{ignore?:(path:string,dirent:fs_p.Dirent)=>boolean;includes?:(path:string,dirent:fs_p.Dirent)=>boolean}} [options] 优先取ignore，其次includes，默认全部过
 * @return {Promise<{files:string[];directories:string[]}>}
 */
async function globRead(dir, options) {
    const info = { files: [], directories: [] };
    await readdirRecursive(dir, info, options);
    return info;
}

function readdirRecursive(dir, info, options) {
    return fs_p.readdir(dir, readdirOptions).then(async (dirents) => {
        for (let dirent of dirents) {
            const name = path.resolve(dir, dirent.name);
            if (
                options &&
                ((options.ignore && options.ignore(name, dirent)) ||
                    (options.includes && !options.includes(name, dirent)))
            )
                continue;
            if (dirent.isDirectory()) {
                info.directories.push(name);
                await readdirRecursive(name, info);
            } else if (dirent.isFile()) {
                info.files.push(name);
            }
        }
    });
}

// globRead(process.cwd(), { ignore: (p) => p.includes('node_modules') })
//     .then((info) => {
//         fs_p.writeFile(
//             path.resolve(process.cwd(), 'glob.json'),
//             JSON.stringify(info),
//         );
//     })
//     .catch((error) => {
//         console.log('error:', error);
//     });

/**
 * @name 拷贝目录到另一个目录
 * @description 拷贝的必须是目录，会拷贝软链接
 * @param {string} from 背靠背目录路径
 * @param {string} to 存放目录
 * @param {{ignore?:(path:string,dirent?:fs_p.Dirent)=>boolean;includes?:(path:string,dirent?:fs_p.Dirent)=>boolean}} [options] 优先取ignore，其次includes，默认全部过
 * @return {Promise<void>}
 */
function globCopyDir(from, to, options) {
    if (
        options &&
        ((options.ignore && options.ignore(from)) ||
            (options.includes && !options.includes(from)))
    )
        return Promise.resolve();
    return fs_p.readdir(from, readdirOptions).then(async (dirents) => {
        await fs_p.mkdir(to).catch(() => {
            console.log('创建父目录:');
        });
        for (let dirent of dirents) {
            const fromPath = path.resolve(from, dirent.name);
            const toPath = path.resolve(to, dirent.name);
            if (
                options &&
                ((options.ignore && options.ignore(fromPath, dirent)) ||
                    (options.includes && !options.includes(fromPath, dirent)))
            )
                continue;
            if (dirent.isDirectory()) {
                await fs_p.mkdir(toPath).catch(() => {
                    console.log('创建子目录:');
                });
                await globCopyDir(fromPath, toPath, options);
            } else if (dirent.isFile()) {
                const file = await fs_p.readFile(fromPath);
                await fs_p.writeFile(toPath, file);
            } else if (dirent.isSymbolicLink()) {
                const target = await fs_p.readlink(fromPath);
                await symlinkOrCopySync(target, toPath);
            }
        }
    });
}

globCopyDir(
    path.resolve(process.cwd(), 'node_modules'),
    path.resolve(process.cwd(), 'node_modules222'),
)
    .then(() => {
        console.log('拷贝成功:');
    })
    .catch((error) => {
        console.log('拷贝失败:');
        console.log(error);
    });
// fs_e.copy(
//     path.resolve(process.cwd(), 'node_modules'),
//     path.resolve(process.cwd(), 'node_modules222'),
//     {},
// )
//     .then(() => {
//         console.log('拷贝成功:');
//     })
//     .catch((error) => {
//         console.log('拷贝失败:', error);
//     });

function testCanSymlink() {
    // 能不能写入文件、创建目录
    const result = {
        files: false,
        directories: false,
    };
    const tmpdir = os.tmpdir();
    const canLinkSrc = path.join(tmpdir, 'zzcanLinkSrc.tmp');
    const canLinkDest = path.join(tmpdir, 'zzcanLinkDest.tmp');
    try {
        fs.writeFileSync(canLinkSrc, '');
    } catch (e) {
        return result;
    }
    try {
        fs.symlinkSync(canLinkSrc, canLinkDest);
        result.files = true;
    } catch (e) {}
    try {
        fs.unlinkSync(canLinkSrc);
        fs.unlinkSync(canLinkDest);
        fs.rmSync(canLinkSrc);
    } catch (e) {}
    try {
        fs.mkdirSync(canLinkSrc);
    } catch (e) {
        result.directories = false;
        return result;
    }
    try {
        fs.symlinkSync(canLinkSrc, canLinkDest, 'dir');
        fs.rmdirSync(canLinkSrc);
        fs.rmdirSync(canLinkDest);
        result.directories = true;
    } catch (e) {
        try {
            fs.rmdirSync(canLinkSrc);
        } catch (e) {}
    }
    return result;
}

// const defaultOptions = {
//     isWindows: process.platform === 'win32',
//     canSymlinkFile: canSymlink.files,
//     canSymlinkDirectory: canSymlink.directories,
//     fs: fs,
// };

// const tmpdir = os.tmpdir();
// const canLinkSrc = path.join(tmpdir, 'zzcanLinkSrc.tmp');
// const canLinkDest = path.join(tmpdir, 'zzcanLinkDest.tmp');
// fs.writeFileSync(canLinkSrc, '');
// fs.symlinkSync(canLinkSrc, canLinkDest);
console.log(':', testCanSymlink());

function symlink(src, target, type) {
    if (IsWindows) {
        if (type === 'junction') {
            return fs_p.symlink(src, target, type);
        }
        return fs_p
            .symlink(src, target, type)
            .catch(() => fs_p.symlink(src, target, 'junction'));
    }
    return fs_p.symlink(src, target, type);
}
