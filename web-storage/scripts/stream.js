require('colors');
const fs = require('fs');
const p_fs = require('fs/promises');
const path = require('path');
const { faker } = require('@faker-js/faker');

const CWD = process.cwd();
const BigTextPath = path.resolve(CWD, '.big_text.json');
const BigTextPath2 = path.resolve(CWD, '.big_text2.json');

async function createBigText() {
    await p_fs.stat(BigTextPath).catch(() => {
        p_fs.writeFile(
            BigTextPath,
            JSON.stringify(
                Array.from({ length: 100000 }).map(() => ({
                    id: faker.string.uuid(),
                    name: faker.person.fullName(),
                    gender: faker.person.gender(),
                    phone: faker.phone.imei(),
                    location: [
                        faker.location.street(),
                        faker.location.city(),
                        faker.location.country(),
                    ].join(','),
                    birthdate: faker.date.anytime(),
                })),
                null,
                '\t',
            ),
        );
    });
}
// async function streamTest() {
//     await createBigText();
//     const stream = fs.createReadStream(BigTextPath);
//     const dest = fs.createWriteStream(BigTextPath2);
//     await new Promise((resolve, reject) => {
//         let done = false;
//         stream.on('data', () => {
//             console.log('读取数据中'.green);
//         });
//         stream.on('end', () => {
//             if (done) return;
//             console.log('读取成功'.green);
//             done = true;
//         });
//         stream.on('close', () => {
//             console.log('stream close:');
//             if (done) return;
//             done = true;
//             resolve();
//         });
//         stream.on('error', (error) => {
//             if (done) return;
//             done = true;
//             reject(error);
//         });
//         stream.pipe(dest);
//         dest.on('finish', () => {
//             console.log('finish:');
//         });
//         dest.on('close', () => {
//             console.log('close:');
//         });
//     })
//         .then(() => {
//             console.log('ssds:');
//         })
//         .catch((error) => {
//             console.log('error:', error);
//         })
//         .finally(() => dest.emit('close'));
// }
// streamTest();

function copyWithSteam(src, dest) {
    const from = fs.createReadStream(src);
    const to = fs.createWriteStream(dest);
    return new Promise((resolve, reject) => {
        to.on('finish', resolve);
        from.on('error', reject);
        to.on('error', reject);
        from.pipe(to);
    });
}

copyWithSteam(BigTextPath, BigTextPath2)
    .then(() => {
        console.log('写入成功:'.green);
    })
    .catch((error) => {
        console.log('写入失败:'.red, error);
    });
