const { IsDev } = require('./constant');

module.exports = {
    async: true,
    devServer: IsDev,
};
