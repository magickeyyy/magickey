const path = require('path');
const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WebpackBar = require('webpackbar');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

const {
    IsDev,
    TITLE,
    OUTPUT_DIR,
    PUBLIC_DIR,
    FilenameTemplate,
    PUBLIC_PATH,
} = require('./constant');

const CommonCssRule = [
    MiniCssExtractPlugin.loader,
    {
        loader: 'css-loader',
        options: { sourceMap: true },
    },
    'postcss-loader',
];

module.exports = {
    mode: IsDev ? 'development' : 'production',
    entry: {
        index: './src/index.ts',
        app: './src/app.ts',
    },
    output: {
        filename: `scripts/${FilenameTemplate}.min.js`,
        path: path.resolve(__dirname, OUTPUT_DIR),
        clean: true,
        publicPath: PUBLIC_PATH,
    },
    devtool: IsDev ? 'cheap-module-source-map' : 'source-map',
    plugins: [
        new HtmlWebpackPlugin({
            title: TITLE,
            template: path.resolve(__dirname, 'template.ejs'),
            publicPath: PUBLIC_PATH,
            inject: 'body',
            chunks: ['index'],
        }),
        new HtmlWebpackPlugin({
            title: TITLE,
            template: path.resolve(__dirname, 'template.ejs'),
            publicPath: PUBLIC_PATH,
            inject: 'body',
            filename: 'app.html',
            chunks: ['app'],
        }),
        new CopyPlugin({
            patterns: [{ from: PUBLIC_DIR }],
        }),
        new MiniCssExtractPlugin({
            filename: `styles/${FilenameTemplate}.min.css`,
        }),
        new WebpackBar(),
        new ForkTsCheckerWebpackPlugin(),
    ],
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: {
                    loader: 'ts-loader',
                    options: {
                        transpileOnly: true,
                    },
                },
                exclude: /node_modules/,
            },
            {
                oneOf: [
                    {
                        test: /\.css$/i,
                        use: [...CommonCssRule],
                    },
                    {
                        test: /\.s[ac]ss$/i,
                        use: [
                            ...CommonCssRule,
                            {
                                loader: 'sass-loader',
                                options: {},
                            },
                        ],
                    },
                    {
                        test: /\.less$/i,
                        use: [
                            ...CommonCssRule,
                            {
                                loader: 'less-loader',
                                options: {},
                            },
                        ],
                    },
                ],
            },
            {
                test: /\.(png|jpe?g|gif|svg|eot|ttf|woff?2)$/i,
                type: 'asset',
            },
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    devServer: {
        port: 3000,
        compress: true,
        client: {
            logging: 'none',
        },
    },
    stats: 'errors-only',
    // optimization: {
    //     emitOnErrors: IsDev,
    //     removeAvailableModules: true,
    //     providedExports: true,
    //     sideEffects: false,
    // },
};
