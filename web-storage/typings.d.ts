/**
 * @name 被默认的JSON.stringify处理后数据不变
 * @description undefined的key会被直接移除；NaN会被转成null，但是从类型上NaN不能从number中识别。所以保留JSON.stringify默认行为
 */
type CanStringifyBase = undefined | null | boolean | number | string;
type CanStringifyArray<T = CanStringifyBase> = (
    | T
    | CanStringifyArray<T>
    | CanStringifyRecord<T>
)[];

/**
 * @name store类型
 * @description 保证store至少是一个{}对象，键值是被序列化后类型不失真（序列化后反序列化，源数据不变。undefined会移除键，NaN会变成null，这些影响不考虑）
 */
interface CanStringifyRecord<T = CanStringifyBase> {
    [K: string]: T | CanStringifyRecord<T> | CanStringifyArray<T>;
}
type StorageType = 'localStorage' | 'sessionStorage';

interface StorageConfig<
    T extends Partial<CanStringifyRecord> = Partial<CanStringifyRecord>,
> {
    namespace: string;
    /**
     * @name 从历史数据计算新数据
     * @description 可能存在历史数据，返回的version、expires、store优先级高于配置
     */
    preState?: (preState: PreState<T>) => PreState<T>;
    type?: StorageType;
    version?: Version;
    storageEvent?: boolean;
    expires?: Expires<T>;
    store?: T;
}

type Expires<T> = number | Partial<Record<keyof T, number>>;

type RecordKey<T extends Record<string, any> = Record<string, any>> = Exclude<
    keyof T,
    symbol | number
>;

type Version<
    Major extends number = number,
    Minor extends number = number,
    Patch extends number = number,
> = `${Major}.${Minor}.${Patch}`;

type DeepReadable<T> = {
    -readonly [K in keyof T]: DeepReadable<T[K]>;
};

type PreState<T> = Pick<MagicStorage<T>, 'expires' | 'version' | 'store'>;

type StoreChangeEvent<T> = (store: T) => void;
