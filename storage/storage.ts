/**
 * @name:
 * @description 字段有前缀，开发环境比较有用，或者接入了第三方库会操作storage避免冲突，或者需要多个独立的storage
 * @description 字段有过期时间
 * @description 有版本号
 * @description localStorage有跨页面通信能力
 */

class MagicStorage {
    /**
     * @name 版本号
     * @description 如果配置了版本号，初始化时只会提取配置的版本号的数据
     */
    #version?: string;
    get version() {
        return this.#version;
    }
    /**
     * @name 字段key前缀
     * @description 默认提取全部storage，配置了前缀，就只会提取以`${prefix}|` 开头的字段
     * @description !!prefix为true才有效
     */
    #prefix?: string;
    get prefix() {
        return this.#prefix;
    }
    #expires?: number;
    get expires() {
        return this.#expires;
    }
    set expires(data: number | undefined) {
        this.#expires = data;
    }
    /**
     * @name storage类型
     * @default localStorage
     * @description 只能初始化时配置
     */
    #type: 'localStorage' | 'sessionStorage' = 'localStorage';
    get type() {
        return this.#type;
    }
    #subsicibeStoageEvent = false;
    /**
     * @name 是否监听storage事件
     * @description 如果是localStorage，开启多个页面时，可能需要监听其他页面的数据变化。
     */
    get subsicibeStoageEvent() {
        return this.#subsicibeStoageEvent;
    }
    set subsicibeStoageEvent(data: boolean) {
        this.#subsicibeStoageEvent = data;
    }
    constructor(config: { version?: string; subsicibeStoageEvent?: number }) {}
}
