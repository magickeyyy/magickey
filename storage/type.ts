type RetainByValueType<T, P> = {
    [K in keyof T]: T[K] extends P
        ? T[K]
        : T[K] extends object
        ? RetainByValueType<T[K], P>
        : never;
};
/**
 * @name 基本数据类型
 * @description obejct的value是基本类型，经过JOSN.parse(JSON.stringif(object))之后还能保留key的类型
 * @description 不管有toSting方法的类型，如Date
 * @description NaN会转成null存储
 * @description undefined、function的键会被移除
 */
type BaseValueType = null | string | number;

type ReturnByStringify<T extends Record<string, any>> = RetainByValueType<
    T,
    BaseValueType
>;
class P {
    name: string = '';
    id() {
        return Math.random();
    }
}
const store = {
    // a: '',
    // b: 1,
    // c: () => 0,
    // d: [1, 2, 3, 'a'],
    // e: {
    //     a: 1,
    //     b: '',
    // },
    f: new Set([, 1, 3]),
    g: new Map(),
    h: new Date(),
    i: undefined,
    p: new P(),
};

type A = ReturnByStringify<typeof store>;
type ArrayType<T> = T extends readonly [...infer R] ? R : never;
type Arr = ArrayType<readonly string[]>;

export { ArrayType };
