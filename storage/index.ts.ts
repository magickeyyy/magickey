export type MagicStorageType = 'local' | 'session';
export type MagicStorageParams<T extends Record<string, any>> = {
    prefix?: string;
    type?: MagicStorageType;
    space?: string;
    version?: string;
    init?: () => T;
    exprire?: Record<keyof T, number>;
};
export interface SetFn<T extends Record<string, any>> {
    (name: Partial<T>): void;
    (name: Key<T>, value?: Partial<T>[typeof name]): void;
}
export type Callback<T extends Record<string, any>> = (store: T) => void;
export type Key<T extends Record<string, any>> = Exclude<
    keyof T,
    symbol | number
>;

/**
 * @name 参数是不是字符串
 */
export function isString(data: unknown): data is string {
    return typeof data === 'string';
}
export function isFunction(data: unknown): data is (...args: any) => any {
    return typeof data === 'function';
}
/**
 * @name 参数是不是{}
 */
export function isStrictObject(data: unknown): data is Record<string, any> {
    return Object.prototype.toString.call(data) === '[object Object]';
}
/**
 * @name 把参数序列化后解析
 */
export function parseValue(value: unknown): [string, any] {
    const str = JSON.stringify(value);
    return [str, JSON.parse(str)];
}
export const VersionRegExp = /^\d+\.\d+\.\d+$/;

/**
 * @name 比较版本
 * @description 返回true表示now比old大
 * @return {boolean}
 */
export function compareVersion(now: string, old: string) {
    const prev = old.split('.');
    const curv = now.split('.');
    return curv.reduce((p, c, i) => {
        if (p) return p;
        return Number(c) > Number(prev[i]);
    }, false);
}
/**
 * @name 校验本地存储是否可用或者有空间
 * @description 摘自MDN
 */
export function storageAvailable(type: 'sessionStorage' | 'localStorage') {
    const storage = window[type];
    try {
        const x = '@__storage_test__@';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    } catch (e) {
        return (
            e instanceof DOMException &&
            // everything except Firefox
            (e.code === 22 ||
                // Firefox
                e.code === 1014 ||
                // test name field too, because code might not be present
                // everything except Firefox
                e.name === 'QuotaExceededError' ||
                // Firefox
                e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
            // acknowledge QuotaExceededError only if there's something already stored
            storage &&
            storage.length !== 0
        );
    }
}

export function isValidNumber(data: unknown): data is number {
    return typeof data === 'number' && !Number.isNaN(data);
}

export function escapeStr(data: string) {
    return data.replace(/[\\"'()[\]{}|]/, '\\$&');
}

/**
 * @name 封装Storage
 * @description
 * * 间隔符 （必须）第一段是前缀，第二段是版本，固定的。第三段如果是纯数字初始化时会被当成过期时间，比当前时间戳大的才会保留，否则清除；剩下的是键。
 * * 前缀 （必须） key第一段，每个key都会带前缀
 * * 版本 （必须） key第二段，每个key都会带版本，自动清理历史版本数据。如果要保留历史版本数据，请在初始化时传入init函数
 * * 过期时间 （非必须）（时间戳）如果初始化时配置了过期时间，会以exprire的为准，不满足的都删除；如果没配置exprire，storage中没有带exprire的，就保留第一个，如果有exprire，就取exprire最高的那个，其一删除
 * @return {*}
 */
class MagicStorage<T extends Record<string, any>> {
    #store: T = {} as T;
    get store() {
        return { ...this.#store };
    }
    #type: MagicStorageType = 'local';
    /**
     * @name 类型
     * @description localStorage或sessionStorage
     * @default 'local'
     */
    get type() {
        return this.#type;
    }
    #prefix: string = '@@';
    /**
     * @name 间隔符
     * @description 只有以间隔符开头的才会计入store
     * @default '@@' 默认空字符串，即全部采集
     */
    get prefix() {
        return this.#prefix;
    }
    #version = '1.0.0';
    /**
     * @name 版本
     * @description 必须是x.x.x格式字符串，在key的第二段。默认会在初始化时清理过往版本数据
     * @default '1.0.0'
     */
    get version() {
        return this.#version;
    }
    #space: string = '|';
    /**
     * @name 间隔符
     * @description 前缀、过期时间、键名之间会依次添加间隔符
     * @default '|'
     */
    get space() {
        return this.#space;
    }
    /**
     * @name sessionStorage或者localStorage
     */
    get storage() {
        return this.#type === 'local' ? localStorage : sessionStorage;
    }
    /**
     * @name 有多少个键
     * @description Storage.length
     */
    get length() {
        return this.storage.length;
    }
    #exprire: Record<keyof T, number> = {} as Record<keyof T, number>;
    /**
     * @name 过期时间
     * @description 每个key一个过期时间；值小于等于0，会直接删掉key中的exprire；如果过期时间小于当前时间会清理store，同时拦截之后的set；值大于当前时间，会更新key。
     */
    get exprire() {
        return { ...this.#exprire };
    }
    get available() {
        return storageAvailable((this.#type + 'Storage') as any);
    }
    #callbacks: Callback<T>[] = [];
    #timer?: NodeJS.Timeout;
    constructor(config?: MagicStorageParams<T>) {
        if (isString(config?.prefix)) {
            this.#prefix = config!.prefix;
        }
        if (isString(config?.type)) {
            this.#type = config!.type;
        }
        if (isString(config?.space)) {
            this.#space = config!.space;
        }
        if (isString(config?.version) && VersionRegExp.test(config!.version)) {
            this.#version = config!.version;
        }
        if (isStrictObject(config?.exprire)) {
            this.#exprire = Object.assign(this.#exprire, config!.exprire);
        }
        this.#store =
            typeof config?.init === 'function' ? config.init() : this.#init();
        this.#runCallback();
    }
    #init = () => {
        if (!this.available) return {} as T;
        const now = Date.now();
        const prefix = this.#getSign();
        return Object.entries(this.storage).reduce((pre, [key, value]) => {
            if (key.startsWith(this.prefix)) {
                const rest = key.replace(this.#getSign(), '');
                const keys = key.replace(this.#getSign(), '').split(this.space);
                const name = keys.slice(-1)[0] as keyof T;
                if (
                    new RegExp(`^\\d+${escapeStr(this.#space)}.+$`).test(rest)
                ) {
                    const [exprireStr, ...names] = rest.split(this.#space);
                    const exprire = this.#exprire[names.join(this.#space)];
                    if (exprire !== undefined && exprireStr !== exprire + '') {
                    }
                }
                const exprire =
                    keys.length === 4 && keys[2] && /^\d+/.test(keys[2])
                        ? Number(keys[2])
                        : undefined;
                if (exprire && exprire <= now) return pre;
                if (exprire) {
                    if (exprire <= now) return pre;
                    if (!this.#exprire[name]) this.#exprire[name] = exprire;
                }
                const version =
                    keys.length === 4 &&
                    [keys][1] &&
                    VersionRegExp.test(keys[1])
                        ? keys[1]
                        : undefined;
                if (version !== this.#version) return pre;
                const va = JSON.parse(value);
                if (va !== undefined) {
                    pre[name] = va;
                }
            }
            return pre;
        }, {} as T);
    };
    #setItem = (name: Key<T>, value: any) => {
        if (!name.trim()) throw new Error(`${name}.trim() is empty`);
        if (name.includes(this.#space)) {
            throw new Error(`${name} should not include ${this.#space}`);
        }
        if (/^\d+$/.test(name)) throw new Error(`${name} should not be number`);
        // 有过期时间
        if (this.#exprire[name] <= Date.now()) return;
        const [str, parse] = parseValue(value);
        if (parse === undefined) {
            this.#delete(name);
        } else {
            this.#store[name as keyof T] = parse;
            this.storage.setItem(this.#switchKey(name), str);
        }
        this.#runCallback();
    };
    #switchKey = (name: string) => {
        return name.startsWith(this.#getSign(name as Key<T>))
            ? name.replace(this.#getSign(name as Key<T>), '')
            : this.#getSign(name as Key<T>) + name;
    };
    #delete = (name: Exclude<keyof T, symbol | number>) => {
        delete this.#store[name];
        this.storage.removeItem(this.#switchKey(name));
        this.#runCallback();
    };
    /**
     * @name 加在key之前的内容
     * @description 传了name可能会加上exprire，否则只有prefix|version|
     */
    #getSign(name?: Key<T>) {
        let str = `${this.#prefix}${this.space}${this.version}${this.space}`;
        if (name && this.#exprire[name] > 0) {
            str += this.#exprire[name] + this.#space;
        }
        return str;
    }
    #supplementExpire = (exprire: string | number) => {
        return `${this.prefix}${this.#space}${this.#version}${
            this.#space
        }${exprire}${this.#space}`;
    };
    #runCallback = () => {
        if (isValidNumber(this.#timer)) clearTimeout(this.#timer);
        this.#timer = setTimeout(() => {
            this.#callbacks.forEach((callback) => {
                callback(this.#store);
            });
            this.#timer = undefined;
        });
    };
    /**
     * @name 从字符串中获取过期时间、键
     * @param {string} name storage中的key
     */
    #keyInfo = (name: string) => {
        const info: { exprire?: string; key?: string } = {};
        const prefix = this.#getSign();
        if (!name.startsWith(prefix)) return info;
        let key = name.replace(prefix, '');
        if (!this.#exprire[key]) {
            info.key = key;
        }
        // ! 这里的前提是先setExprire再set
        if (this.#exprire[key]) {
            const [exprire, ...rest] = key.split(this.#space);
            info.exprire = exprire;
            info.key = rest.join(this.#space);
        } else {
            info.key = key;
        }
        return info;
    };
    /**
     * @name 根据store的key找storage中的key
     */
    #findKeyByName = (name: string) => {
        return Object.keys(this.storage).find(
            (key) => key.endsWith(name) && key.startsWith(this.#getSign()),
        );
    };
    /**
     * @name 获取存储值
     * @description 如果此键有过期时间并且已过期，会删除此键
     * @param {string} name store的key
     */
    get = (name: Key<T>) => {
        const exprire = this.#exprire[name];
        if (exprire && Date.now() >= exprire) {
            this.#delete(name);
            return undefined;
        }
        return this.#store[name];
    };
    /**
     * @name 设置或更新
     * @description
     * @param {string} name store的key，会根据value更新或删除键；对象，与store合并
     * @param {any} value 显式的undefined会删除store[name]并移除storage[key]
     * @return {void}
     */
    set: SetFn<T> = (name, value?: any) => {
        if (isString(name)) {
            this.#setItem(name, value);
        } else if (isStrictObject(name)) {
            Object.entries(name).forEach(([k, v]) => {
                this.#setItem(k as Key<T>, v);
            });
        }
    };
    /**
     * @name 移除单个或多个键
     * @param {sring[]|string} name
     */
    delete = (name: Key<T>[] | Key<T>) => {
        if (isString(name)) {
            this.#delete(name);
        } else if (Array.isArray(name)) {
            name.forEach((n) => {
                if (isString(n)) {
                    this.#delete(n);
                }
            });
        }
    };
    /**
     * @name 设置键的过期时间
     * @description 请确保键值是非NaN的整数
     * * 如果键值小于等于0，会移除过期时间配置，同时移除storage的key中的过期时间
     * * 如果键值大于当前时间，会检索storage的key，如果没过期时间会加上，否增更新；如果小于当前时间，会直接移除
     * * 如果此时还没有这个键，会等到下次set时执行以上逻辑
     */
    setExprire = (data: Record<keyof T, number>) => {
        let changed = false;
        Object.entries(data).forEach(([name, value]) => {
            if (!isValidNumber(value)) return;
            const origin = this.#findKeyByName(name);
            if (this.#store[name] === undefined || !origin) {
                this.#exprire[name as keyof T] = value;
                return;
            }
            const { exprire, key } = this.#keyInfo(origin);
            if (value <= 0) {
                if (exprire) {
                    const pre = this.storage.getItem(origin);
                    this.storage.removeItem(origin);
                    pre != undefined &&
                        this.storage.setItem(`${this.#getSign()}${key}`, pre);
                }
            } else {
                if (exprire !== this.#exprire[name as keyof T] + '') {
                    const pre = this.storage.getItem(origin);
                    this.storage.removeItem(origin);
                    if (value <= Date.now()) {
                        delete this.#store[name];
                        !changed && (changed = true);
                    } else {
                        pre !== null &&
                            this.storage.setItem(
                                `${this.#getSign()}${exprire}${
                                    this.#space
                                }${key}`,
                                pre,
                            );
                    }
                }
            }
            this.#exprire[name as keyof T] = value;
        });
        changed && this.#runCallback();
    };
    on = (callback: Callback<T>) => {
        const index = this.#callbacks.findIndex((fn) => fn === callback);
        if (index) {
            this.#callbacks.splice(index, 1);
        }
        this.#callbacks.push(callback);
    };
    off = (callback: Callback<T>) => {
        const index = this.#callbacks.findIndex((fn) => fn === callback);
        if (index) {
            this.#callbacks.splice(index, 1);
        }
    };
    once = (callback: Callback<T>) => {
        const cb: Callback<T> = (store) => {
            callback(store);
            this.off(cb);
        };
        this.#callbacks.push(cb);
    };
}

const local = new MagicStorage();
local.on((store) => {
    console.log('store:', store);
});
local.setExprire({
    ct: new Date('2023/03/28').valueOf(),
    a: Date.now(),
    b: new Date('2023/8/28').valueOf() + 1000 * 10,
});
local.set('a', 1);
local.set({ b: 12, ct: { a: 1 } });
local.delete('a');
local.delete('b');
local.set('b', 33);
setTimeout(() => {
    console.log('get:', local.get('b'));
}, 2000);
setTimeout(() => {
    console.log('get:', local.get('b'));
}, 10000);

export { MagicStorage };
