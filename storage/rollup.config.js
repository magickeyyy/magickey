const path = require('path');
const fs = require('fs/promises');
const { rollup } = require('rollup');
const typescript = require('rollup-plugin-typescript2');
const { terser } = require('rollup-plugin-terser');

(async () => {
    const bundle = await rollup({
        input: path.resolve(__dirname, 'index.ts'),
        plugins: [
            typescript({
                tsconfigOverride: {
                    exclude: [
                        ,
                        './dist',
                        './rollup.config.js',
                        './jest.config.ts',
                    ],
                },
            }),
            terser(),
        ],
    });
    await fs
        .rm(path.resolve(__dirname, 'dist'), { recursive: true })
        .catch(() => {});
    await bundle.write({
        format: 'commonjs',
        file: path.resolve(__dirname, 'dist/index.cjs'),
        sourcemap: true,
    });
    await bundle.write({
        format: 'esm',
        file: path.resolve(__dirname, 'dist/index.js'),
        sourcemap: true,
    });
})();
