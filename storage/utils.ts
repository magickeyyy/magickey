/**
 * @name:
 * @description 原生不支持代理Map、Set，使用代理对象的成员this指向不一定正确
 */
function proxy<T extends object>(data: T): T {
    const tree = [];
    return new Proxy<T>(data, {
        get: function (target, key, receiver) {
            return Reflect.get(target, key, receiver);
        },
        set: function (target, key, value, receiver) {
            if (Array.isArray(value) || isStrictObject(value)) {
                return Reflect.set(target, key, proxy(value), receiver);
            }
            return Reflect.set(target, key, value, receiver);
        },
    });
}
/**
 * @name 参数是不是被JSON.stringif序列化后还会保留原样
 * @description NaN会被序列化成null，保留了key，所以保留，只是ts没这个类型
 * @description Infinity会被序列化成null,BigInity会报错
 * @description Symbol没发存，因为每个Symbol都是唯一的
 */
function isBaseValue(data: unknown): data is null | string | number {
    return (
        data === null ||
        Number.isNaN(data) ||
        typeof data === 'number' ||
        typeof data === 'string'
    );
}
/**
 * @name:
 * @description
 * ! 不能区分class和{}
 */
function isStrictObject(data: unknown): data is Record<string, any> {
    return Object.prototype.toString.call(data) === '[object Object]';
}

/**
 * @name 只保留值是null/NaN/string/number的stringify函数
 * @description JSON.stringify时，会调用toString转化，像Date、Set、Map等就变样了，不能保留影响数据类容
 */
function stringify(data: unknown): any {
    return JSON.stringify(data, function (key: string, value: any) {
        if (isBaseValue(value)) {
            return value;
        } else if (Array.isArray(value)) {
            return value;
        } else if (isStrictObject(value)) {
            return value;
        }
    });
}
function clone<T extends Record<string, any>, P>(
    data: T,
    condition: (data: any) => boolean,
): P {
    if (Array.isArray(data)) {
        return data.map((item) => {
            const v = condition(item) ? item : clone(item, condition);
            return v;
        }) as P;
    } else if (isStrictObject(data)) {
        return Object.entries(data).reduce((pre, [key, value]) => {
            const v = condition(value) ? value : clone(value, condition);
            if (v !== undefined) {
                pre[key] = v;
            }
            return pre;
        }, {} as any) as P;
    }
    return undefined as P;
}

export { proxy, stringify, clone };
